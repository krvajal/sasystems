﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using sa_systems.resources.controls;
using System.Windows.Media.Animation;
using sa_systems.models;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Calendar.xaml
    /// </summary>
    /// 
    
    public partial class CalendarPage : Page
    {

        private System.Globalization.Calendar calendar;
        private Int32 month; //month shown in calendar
        private Int32 year; // year shown in calendar
        private Int32 day;  // day selected in calendar
        private Int32 currentMonth; //actual month
        private Int32 currentYear;
        private Int32 currentDay;
        private bool _isDaySelected = false; //
        private Int32 selectedEndDay;
        private Int32 selectedEndDayIndex;
        int startMonthIndex;
        int endMonthIndex;
        private List<RoundedRectangle> rects;
        Ellipse startDayEllipse;

        public bool IsDaySelected {
            set {
                _isDaySelected = true;
                ShowDayMarkers();
            }
        }

        private void ShowDayMarkers()
        {

            foreach(RoundedRectangle r in rects) {
                if (this.CalendarGrid.Children.Contains(r)) { //check also for labels
                    this.CalendarGrid.Children.Remove(r);
                }
            }
            //todo, remove from grid first
            rects.Clear();

            int startRow = 1;
            int endRow = this.selectedEndDayIndex/ 7 ;
            int startCol = 0;
            int endCol = this.selectedEndDayIndex %7;
            var sb = new Storyboard();
            var duration = TimeSpan.FromMilliseconds(200); //duraction of one cell animation

            if (DaysBetween(DateTime.Now, new DateTime(this.year, this.month, this.selectedEndDay)) < (this.selectedEndDayIndex - 7)) {
                //the start day is shown in current month view
                if (this.month != currentMonth)
                {
                    //show at the begining, so start row is 1
                     startRow = 1; // as the day is before the current month

                    //to get column just get day of week
                    startCol = this.calendar.GetDayOfWeek(DateTime.Now) - DayOfWeek.Monday;
                    if (startCol == -1) {  //sunday case
                        startCol = 6;
                    }
                }
                else {
                    int startDayIndex = startMonthIndex + currentDay - 1;
                    startCol = startDayIndex % 7;
                    startRow = startDayIndex / 7 + 1;
                }
            }

           

            if (startRow != endRow)
            {
                //////////////////////
                // this need urgent refactoring
                //show selected day marker
                var rect = new RoundedRectangle();
                rect.SetValue(Grid.RowProperty, endRow);
                rect.SetValue(Grid.ColumnProperty, 0);
                rect.SetValue(Grid.ColumnSpanProperty, endCol + 1);
                rect.HorizontalAlignment = HorizontalAlignment.Center;
                rect.Width = (endCol - 1) * 171 + 171 * 1.3;
                rect.SetValue(Panel.ZIndexProperty, -1);
                this.CalendarGrid.Children.Add(rect);
                rects.Add(rect);

                for (int rowi = endRow - 1; rowi > startRow; rowi--)
                {
                    var rectangle = new RoundedRectangle();
                    rectangle.SetValue(Grid.RowProperty, rowi);
                    rectangle.SetValue(Grid.ColumnProperty, 0);
                    rectangle.SetValue(Grid.ColumnSpanProperty, 7);
                    rectangle.HorizontalAlignment = HorizontalAlignment.Center;
                    rectangle.Width = 5 * 171 + 171 * 1.3;

                    rectangle.SetValue(Panel.ZIndexProperty, -1);
                    this.CalendarGrid.Children.Add(rectangle);
                    rects.Add(rectangle);
                }


                //show start day marker
                //show selected day marker
                var rect1 = new RoundedRectangle();
                rect1.SetValue(Grid.RowProperty, startRow);
                rect1.SetValue(Grid.ColumnProperty, startCol);
                rect1.SetValue(Grid.ColumnSpanProperty, 7 - startCol + 1);
                rect1.HorizontalAlignment = HorizontalAlignment.Center;
                rect1.Width = (7 - startCol - 1 - 1) * 171 + 171 * 1.3;
                rect1.SetValue(Panel.ZIndexProperty, -1);
                this.CalendarGrid.Children.Add(rect1);
                rects.Add(rect1);
            }
            else {
                //same rows
                var rect = new RoundedRectangle();
                rect.SetValue(Grid.RowProperty, endRow);
                rect.SetValue(Grid.ColumnProperty, startCol);
                rect.SetValue(Grid.ColumnSpanProperty, endCol  - startCol + 1);
                rect.HorizontalAlignment = HorizontalAlignment.Center;
                rect.Width = (endCol - startCol - 1) * 171 + 171 * 1.3;
                rect.SetValue(Panel.ZIndexProperty, -1);
                this.CalendarGrid.Children.Add(rect);
                rects.Add(rect);

            }
            //now animate rects
            AnimateRects();


        }
        void AnimateRects() {


            var sb = new Storyboard();
            int i = 0;
            var cellAnimationDuration = TimeSpan.FromMilliseconds(100);
            double cellWidth = 177.1;
            var delay = TimeSpan.FromMilliseconds(0);
            foreach (var rect in rects) {
                double targetWidth = rect.Width + 60.0;
                var duration = TimeSpan.FromMilliseconds(cellAnimationDuration.Milliseconds * targetWidth/ cellWidth);
                
                var animation = new DoubleAnimation
                {
                    From = 0,
                    To = targetWidth,
                    Duration = duration,
                    BeginTime = delay
                };
                delay = delay + duration;
                Storyboard.SetTarget(animation, rect);
                Storyboard.SetTargetProperty(animation, new PropertyPath("Width"));
                sb.Children.Add(animation);
                rect.HorizontalAlignment = HorizontalAlignment.Right;
                rect.Width = 0;
            }
            sb.Completed += RunExitAnimation;
            sb.Begin();

            TravelInfo.GetCurrentTravel().SetStartDate( DateTime.Now);
            
           
        }

        private List<Label> labels = new List<Label>(); //keep record of the month labels added to calendar
        public CalendarPage()
        {
            this.calendar = new System.Globalization.GregorianCalendar();

            DateTime now = DateTime.Now;
            int day = this.calendar.GetDayOfMonth(now);
            this.month = now.Month;
            this.year = now.Year;
            this.currentMonth = this.month;
            this.currentYear = this.year;
            this.currentDay = now.Day;

            this.rects = new List<RoundedRectangle>();
            InitializeComponent();

            this.PrevButton.IsEnabled = false;

            startDayEllipse = GenerateDayMarkerEllipse();
            StartDateLabel.Text  = now.ToString("dddd,\nMMMM d", CultureInfo.InvariantCulture );
            

            //start with current month
            UpdateCalendar(this.month, this.year);
            
        }

        void UpdateCalendar(Int32 month, Int32 year) {


          
            RemoveLabelsFromCalendar();
            //this needs to be moved to a resource
            //TODO: refactor this
            //Gray color: #2c4048
            var grayColor = new Color();
            grayColor.R = 44;
            grayColor.G = 64;
            grayColor.B = 72;
            grayColor.A = 255;
            var grayBrush = new SolidColorBrush(grayColor);


            // get starting day
      
            DayOfWeek dow = this.calendar.GetDayOfWeek(new DateTime(year, month, 1));
            int start = dow - DayOfWeek.Monday;
            if (start == -1) start = 6; // because sunday is before monday
            startMonthIndex = start;

            int numDays = this.calendar.GetDaysInMonth(year, month);
            endMonthIndex = startMonthIndex + numDays;
            for (int i = 0; i < numDays; i++) {
                var dayLabel = new Label();
                string day = (1 + i).ToString();
                if (i == 0) {
                    string monthName = new DateTime(year, month, 1)
                    .ToString("MMM", CultureInfo.InvariantCulture);
                    day = monthName + " " + day;
                }
                int col = (i + start) % 7  ;
                int row = (i + start) / 7 + 1;
                if (row >= 6) {
                    continue;
                }
                dayLabel.Content = day;
                bool inactive = (this.month == currentMonth) && ((1 + i) <= this.currentDay); 
                AddLabel(dayLabel, row, col, (inactive)?(grayBrush):null);
                if (this.month == currentMonth && this.currentDay == (1 + i)) {
                    if (this.CalendarGrid.Children.Contains(startDayEllipse)) {
                        this.CalendarGrid.Children.Remove(startDayEllipse);

                    }
                    startDayEllipse.SetValue(Grid.RowProperty, row);
                    startDayEllipse.SetValue(Grid.ColumnProperty, col);
                    this.CalendarGrid.Children.Add(startDayEllipse);
                }


            }
            // fill previous month
            if (start > 0) {
                //we have days before, so fill them up
                int prevMonth = (month > 1) ? (month - 1) : 12;
                int prevYear = (prevMonth != 12) ? year : (year - 1);
                int numDaysPrevMonth = this.calendar.GetDaysInMonth(prevYear, prevMonth);
                int startDay = numDaysPrevMonth - (start - 1);
                for (int i = 0; i < start; i++) {
                    int col = i;
                    int row = 1;
                    int day = startDay + i;
                    Label dayLabel = new Label();
                    dayLabel.Content = day.ToString();
                    AddLabel(dayLabel, row, col, grayBrush);
                    if (prevMonth == currentMonth && this.currentDay == day)
                    {
                        if (this.CalendarGrid.Children.Contains(startDayEllipse))
                        {
                            this.CalendarGrid.Children.Remove(startDayEllipse);

                        }
                        startDayEllipse.SetValue(Grid.RowProperty, row);
                        startDayEllipse.SetValue(Grid.ColumnProperty, col);
                        this.CalendarGrid.Children.Add(startDayEllipse);
                    }
                }
               


            }
            //fill next month
            int remainingSlots = (this.CalendarGrid.RowDefinitions.Count - 1) * (this.CalendarGrid.ColumnDefinitions.Count) - numDays - start ;
            if (remainingSlots > 0) {
                int nextMonthDaysStartIndex = numDays + start;
                for (int i = 0; i < remainingSlots; i++) {
                    var dayLabel = new Label();

                    string day = (i + 1).ToString();
                    if (i == 0) {
                        //first day, prepend moth
                        string monthName = new DateTime(year, month, 1).AddMonths(1)
                        .ToString("MMM", CultureInfo.InvariantCulture);
                        day = monthName + " " + day;
                       
                    }
                    dayLabel.Content = day;
                    int row = this.CalendarGrid.RowDefinitions.Count - 1; //last row, obviously
                    int col = (start + numDays + i) % 7;
                    AddLabel(dayLabel, row, col,grayBrush);

                }
            }

            //update labels at the top
            this.CalendarHeaderLabel.Content = new DateTime(this.year, this.month, 1).ToString("MMMM yyyy", CultureInfo.InvariantCulture);

        }


        //add the month label in the right spot in the calendar
        private void AddLabel(Label dayLabel, int row, int col, SolidColorBrush brush = null)
        {
            bool activeLabel = (brush == null);
            if (brush == null)
            {
                brush = Brushes.White;
            }

            dayLabel.SetValue(Grid.RowProperty, row);
            dayLabel.SetValue(Grid.ColumnProperty, col);
            dayLabel.HorizontalAlignment = HorizontalAlignment.Center;
            dayLabel.VerticalAlignment = VerticalAlignment.Center;
            dayLabel.SetValue(Label.FontSizeProperty, 28.0);
            dayLabel.Foreground = brush;
            if (activeLabel)
            {
                dayLabel.MouseDown += LabelClicked;
            }
            this.CalendarGrid.Children.Add(dayLabel);
            labels.Add(dayLabel);
        }

        private void LabelClicked(object sender, MouseButtonEventArgs e)
        {
            if (this._isDaySelected == false)
            {
                var label = sender as Label;
                Int32 row = (Int32)label.GetValue(Grid.RowProperty);
                Int32 col = (Int32)label.GetValue(Grid.ColumnProperty);
                selectedEndDayIndex = row * 7 + col;
                var ellipse = GenerateDayMarkerEllipse();
               
                ellipse.SetValue(Grid.RowProperty, row);
                ellipse.SetValue(Grid.ColumnProperty, col);
               
                this.CalendarGrid.Children.Add(ellipse);
                this.selectedEndDay = Int32.Parse(label.Content as String);

                var endDate = new DateTime(this.year, this.month, this.selectedEndDay);
                ShowDayMarkers();
                this.EndDateLabel.Text = endDate.ToString("dddd,\nMMMM d",CultureInfo.InvariantCulture);
                TravelInfo.GetCurrentTravel().SetEndDate(endDate);

                _isDaySelected = true;
                
            }
        }

        private void NavigateNextMonth(object sender, RoutedEventArgs e)
        {
          
            month = (month == 12) ? 1 : month + 1;
            year = (month != 1) ? year : (year + 1);
            UpdateCalendar(month, year);
            this.PrevButton.IsEnabled = true;
       
        }

        private void NavigatePrevMonth(object sender, RoutedEventArgs e)
        {
            if (this.month == this.currentMonth)
            {
                //cant go backwards
            }
            else {
                month = (month > 1) ? (month - 1) : 12;
                year = (month != 12) ? year : (year - 1);
                UpdateCalendar(month, year);
                
            }
            if (this.month == this.currentMonth && this.year == this.currentYear) {
                this.PrevButton.IsEnabled = false;
            }
        }
        void RemoveLabelsFromCalendar() {
            foreach (Label l in labels) {
                this.CalendarGrid.Children.Remove(l);
                    
            }
            labels.Clear(); // the records ar not needed anymore

            //also remover day marker
            if (this.startDayEllipse != null && this.CalendarGrid.Children.Contains(this.startDayEllipse)) {
                this.CalendarGrid.Children.Remove(this.startDayEllipse);

            }
        }

        int DaysBetween(DateTime d1, DateTime d2)
        {
            TimeSpan span = d2.Subtract(d1);
            return (int)span.TotalDays;
        }
        Ellipse GenerateDayMarkerEllipse() {

            var ellipse = new Ellipse();
            ellipse.Stroke = Brushes.White;
            ellipse.StrokeThickness = 2;
            ellipse.Stretch = Stretch.Uniform;
            ellipse.HorizontalAlignment = HorizontalAlignment.Center;
            ellipse.VerticalAlignment = VerticalAlignment.Center;
            ellipse.Width = 70;
            ellipse.Height = 70;
          ;
            ellipse.SetValue(Panel.ZIndexProperty, -2);
            var opaqueBlueColor = new Color {
                R = 37,
                G = 147,
                B = 183,
                A = 60
            };
            ellipse.Fill = new SolidColorBrush(opaqueBlueColor);
            return ellipse;

        }

        void RunExitAnimation(object sender, EventArgs args) {

            var sb = this.PagePanel.FindResource("ExitAnimationStoryboard") as Storyboard;
            sb.Completed += NavigateToNextPage;
            sb.Begin();   


        }
        void NavigateToNextPage(object sender, EventArgs args) {
            TrackRunner.GetInstance().NextQuestion();
        }
    }
}
