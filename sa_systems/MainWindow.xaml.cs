﻿using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;
using sa_systems.resources.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace sa_systems
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window{
     
        TrackRunner _runner;
        private const string FURL = "https://sasystems.firebaseio.com/";
        private const string FSECRET_KEY = "Kfl4ZGV81pgDOATECiEX51jAZd9lEWxrS19E6Wrr";

        public FirebaseClient FClient;
        IFirebaseConfig config;

        public MainWindow()
        {
            InitializeComponent();
            
            //MainWindowCanvas.Children.Add(new TouchKeyboard());
            //MainWindowCanvas.Children.Add(new NumericKeypad());
           

            config = new FirebaseConfig
            {
                AuthSecret = FSECRET_KEY,
                BasePath = FURL
            };

            FClient = new FirebaseClient(config);
            TrackRunner.Init(_mainFrame.NavigationService, TopHeader);
            _runner = TrackRunner.GetInstance();

            //Resources["MainBlueColor"] = Colors.Red;          

        }

        internal NumericKeypad GetNumericKeypad()
        {

            return Keypad;
           
        }

        internal TouchKeyboard GetTouchKeyboard()
        {
            return Keyboard;
        }

       

        private void MainWindowCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            //Point x = e.MouseDevice.GetPosition(this);
            //Console.WriteLine(x.X);
            //Console.WriteLine(x.Y);
            //var touchResposeCtrl = new TouchResponseControl();
            //touchResposeCtrl.Width = 100;
            //touchResposeCtrl.Height = 100;
            //touchResposeCtrl.SetValue(LeftProperty, (x.X - touchResposeCtrl.Width/2)*1.4);
            //touchResposeCtrl.SetValue(TopProperty, (x.Y - touchResposeCtrl.Height/2)*1.4);
            //MainWindowCanvas.Children.Add(touchResposeCtrl);
            //touchResposeCtrl.Blink();

        }

        internal void ShowLoader()
        {
            //throw new NotImplementedException();
        }

        internal void HideLoader()
        {
            //throw new NotImplementedException();
        }
    }
  
}
