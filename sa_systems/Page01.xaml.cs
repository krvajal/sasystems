﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page01.xaml
    /// </summary>
    public partial class Page01 : Page
    {


        private DoubleAnimation slowRotationAnimation = new DoubleAnimation
        {
            By = -30,
            Duration = TimeSpan.FromMilliseconds(5000),
            RepeatBehavior = RepeatBehavior.Forever,
            IsCumulative = true
        };
        private DoubleAnimation fastRotationAnimation = new DoubleAnimation
        {
            By = -(180 + 360),
            Duration = TimeSpan.FromMilliseconds(300),
            IsCumulative = true,
            EasingFunction = new CircleEase()
        };


      

        public Page01()
        {
            Loaded += PageLoaded;
            InitializeComponent();

            this.NormalTrackButton.SetTrackText("UNDER 3 MINUTES                     WITHOUT PASSPORTS");
            this.FastTrackButton.SetTrackText("SAVE 1 MINUTE!                     WITH PASSPORTS");
        }


      
        private void StartNormalTrack(object sender, RoutedEventArgs e)
        {
            var sb = FindResource("StartNormalTrackAnimation") as Storyboard;
            sb.Completed += (a, er) =>
            {
                TrackRunner.GetInstance().StartTrack(TrackType.NormalTrack);
            };
            sb.Begin();

          

        }
        private void PageLoaded(object sender, RoutedEventArgs e ) {

           

        }

        private void StartFastTrack(object sender, RoutedEventArgs e)
        {

            var sb = FindResource("StartFastTrackAnimation") as Storyboard;
            sb.Completed += (a, er) =>
            {
                TrackRunner.GetInstance().StartTrack(TrackType.FastTrack);
            };
            sb.Begin();
        }

        private void StartNormalTrack(object sender, MouseButtonEventArgs e)
        {
            var sb = FindResource("StartNormalTrackAnimation") as Storyboard;
            sb.Completed += (send, arg) =>
            {
                TrackRunner.GetInstance().StartTrack(TrackType.NormalTrack);
            };
            sb.Begin();



        }
    }
}
