﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for AddCoverCircleButton.xaml
    /// </summary>
    public partial class AddCoverCircleButton : UserControl
    {


        string _detailsText;
        bool _showingDetails = false;
        private bool _added = false;

        public AddCoverCircleButton()
        {
            InitializeComponent();
            _detailsText = "Add extra cover for your laptop, tablet or mobile phone with up to £1,000 protection in case of theft, damage or loss while on your trip.\n\n(N.B.This benefit is per policy and not per person).";
            
        }


        public void SetDetailsText(string text) {

            _detailsText = text;

        }
        public bool IsAdded() {
            return _added;
        }
        private void ShowMoreAnimationCompleted(object sender, EventArgs e)
        {
            //Add text here
            if (!_showingDetails)
            {
                DetailsTextBlock.Opacity = 1;
                label.Content = "      -";

                TypewriteTextblock(_detailsText, DetailsTextBlock, TimeSpan.FromMilliseconds(1000));
            }
            else {
                label.Content = "READ MORE";

                DetailsTextBlock.Text = String.Empty;
            }
            _showingDetails = !_showingDetails;
        }

        private void TypewriteTextblock(string textToAnimate, TextBlock txt, TimeSpan timeSpan)
        {
            Storyboard story = new Storyboard();
            story.FillBehavior = FillBehavior.HoldEnd;
            
            
            DiscreteStringKeyFrame discreteStringKeyFrame;
            StringAnimationUsingKeyFrames stringAnimationUsingKeyFrames = new StringAnimationUsingKeyFrames();
            stringAnimationUsingKeyFrames.Duration = new Duration(timeSpan);

            string tmp = string.Empty;
            foreach (char c in textToAnimate)
            {
                discreteStringKeyFrame = new DiscreteStringKeyFrame();
                discreteStringKeyFrame.KeyTime = KeyTime.Paced;
                tmp += c;
                discreteStringKeyFrame.Value = tmp;
                stringAnimationUsingKeyFrames.KeyFrames.Add(discreteStringKeyFrame);
            }
            Storyboard.SetTargetName(stringAnimationUsingKeyFrames, txt.Name);
            Storyboard.SetTargetProperty(stringAnimationUsingKeyFrames, new PropertyPath(TextBlock.TextProperty));
            story.Children.Add(stringAnimationUsingKeyFrames);
            story.Completed += Story_Completed;
            story.Begin(txt);
        }

        private void Story_Completed(object sender, EventArgs e)
        {
            DetailsTextBlock.Text = String.Empty;
        }

        private void ShowMoreButtonClicked(object sender, MouseButtonEventArgs e)
        {
            if (_showingDetails)
            {
                DetailsTextBlock.Text = "";
                var sb = FindResource("ShowLessAnimation") as Storyboard;
                sb.Begin();
            }
            else
            {
                var sb = FindResource("ShowMoreAnimation") as Storyboard;
                sb.Begin();
            }
        }

        private void AddButtonClicked(object sender, MouseButtonEventArgs e)
        {
            if (_added)
            {
                ItemImage.Source = new BitmapImage(new Uri("pack://application:,,,/resources/media/gadgets_icon.png"));
                var sb = FindResource("RemoveCoverAnimation") as Storyboard;
                sb.Completed += RemoveCoverAnimationCompleted;
                sb.Begin();

            }
            else
            {
                 ItemImage.Source = new BitmapImage(new Uri("pack://application:,,,/resources/media/gadgets_icon_white.png"));
                var sb = FindResource("AddCoverAnimation") as Storyboard;
                sb.Completed += AddCoverAnimationCompleted;
                sb.Begin();
            }

            _added = !_added;
        }

    

        private void AddCoverAnimationCompleted(object sender, EventArgs e)
        {

           
           
        }

        private void RemoveCoverAnimationCompleted(object sender, EventArgs e)
        {
            
        }
    }
}
