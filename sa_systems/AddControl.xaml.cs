﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for AddControl.xaml
    /// </summary>
    public partial class AddControl : UserControl
    {

        bool rotated = false;
        public AddControl()
        {
            InitializeComponent();
            this.MouseDown += AddControl_MouseDown;
        }

        private void AddControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!rotated)
            {
                
                
            }
            else {
                var sb = FindResource("CancelAnimationStoryboard") as Storyboard;
                sb.Begin();
            }
            rotated = !rotated;
        }
    }
}
