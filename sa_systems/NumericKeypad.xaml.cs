﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for NumericKeypad.xaml
    /// </summary>
    public partial class NumericKeypad : UserControl
    {

        public EventHandler Done;
        public NumericKeypad()
        {
            InitializeComponent();
        }

        public void Present()
        {
            TypedNumber.Content = "";
            Done = null;
            var sb = FindResource("SlideInFromRightAnimation") as Storyboard;
            sb.Begin();
        }
        public void Dismiss()
        {

            var sb = FindResource("SlideOutToRight") as Storyboard;
            sb.Begin();
        }
        private void ResetClicked(Object sender, RoutedEventArgs e)
        {

            TypedNumber.Content = "";
        }
        private void KeyClicked(Object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string digit = btn.Content as String;
            TypedNumber.Content = (TypedNumber.Content as String) + digit;
        }
        private void OkayClicked(Object sender, RoutedEventArgs e)
        {
            Done?.Invoke(this, e);
            Dismiss();
        }
    }
}
