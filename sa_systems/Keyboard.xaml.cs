﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Keyboard.xaml
    /// </summary>
    public partial class TouchKeyboard : UserControl
    {
        public EventHandler Done;
        public EventHandler Canceled;
        public TouchKeyboard()
        {
            InitializeComponent();
            TypedWord.Content = "";
        }

        private void LetterClicked(object sender, RoutedEventArgs e)
        {
            var b = sender as Button;
            string letter = b.Content as string;
            string word = TypedWord.Content as String;
            word = word + letter; // add letter
            TypedWord.Content = word;

        }
        private void DeleteClicked(object sender, RoutedEventArgs e) {
           
            
            string word = TypedWord.Content as String;
            if(word.Length > 0)
                word = word.Substring(0, word.Length - 1);
            TypedWord.Content = word;
        }

        private void CloseClicked(object sender, RoutedEventArgs e)
        {

            Canceled?.Invoke(this, e);
            var sb = FindResource("SlideDownAnimation") as Storyboard;
            sb.Begin();
           
        }
        public void Present() {
            Done = null;
            SlideUp();
        }
        private void SlideUp() {

            TypedWord.Content = "";
            Done = null;
            var sb = FindResource("SlideUpAnimation") as Storyboard;
            sb.Begin();

        }

        private void ConfirmClicked(object sender, RoutedEventArgs e)
        {
            var sb = FindResource("SlideDownAnimation") as Storyboard;
            sb.Begin();
            Done?.Invoke(this, e);

        }
    }

}
