﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page12.xaml
    /// </summary>
    public partial class Page12 : Page
    {
        public Page12()
        {
            InitializeComponent();
        }

        private void YesButtonClicked(object sender, RoutedEventArgs e)
        {
            // user accepted to purchase
            //without medical conditions
            //perform exit animation
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (ev, arg) => {
                TrackRunner.GetInstance().NextQuestion();
            };
            sb.Begin();
        }
        private void NoButtonClicked(object sender, RoutedEventArgs e) {

            TrackRunner.GetInstance().CancelTrack();

        }

       
    }
}
