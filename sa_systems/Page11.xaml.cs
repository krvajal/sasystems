﻿using sa_systems.resources.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page11.xaml
    /// </summary>
    public partial class Page11 : Page
    {

        bool showingInfoSheet = false;
        MedicalConditionsInfoSheet sheet;
        public Page11()
        {
            InitializeComponent();
            sheet = new MedicalConditionsInfoSheet();
            sheet.Closed += Sheet_Closed;
            MainSectionContainer.Children.Add(sheet);
        }

        private void Sheet_Closed(object sender, EventArgs e)
        {

            showingInfoSheet = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MoreInfoButtonClicked(object sender, RoutedEventArgs e)
        {
            if (!showingInfoSheet) {
                sheet.SlideDown();
            }
        }

        private void YesButtonClicked(object sender, RoutedEventArgs e)
        {
            // user accepted to purchase
            //without medical conditions
            //perform exit animation
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += ExitAnimationCompleted;
            sb.Begin();
        }

        private void ExitAnimationCompleted(object sender, EventArgs e)
        {
            this.NavigationService.Navigate(new Page12());
        }

        private void NoButtonClick(object sender, RoutedEventArgs e)
        {

            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (ev, a) =>
            {
                TrackRunner.GetInstance().NextQuestion();
            };
            sb.Begin();
            
        }
    }

}
