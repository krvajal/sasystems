﻿using FireSharp.Exceptions;
using FireSharp.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using sa_systems.models;
using sa_systems.resources.controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace sa_systems
{

    public enum TrackType { NormalTrack, FastTrack};
    public enum QuestionType {CalendarQuestion = 0,
                             CrewQuestion = 1,
                             DestinationQuestion = 2,
                            PolicyQuestion = 3,
                            ExtraCoverQuestion = 4,
                            MedicalConditionsQuestion = 5, 
                            DetailsQuestion = 6,
                            ContactQuestion = 7,
                            PaymentQuestion = 8,
                            ThanksQuestion  = 9,
                            InfoBubbleQuestion  = 10};


    public enum TabType { None = 0 , Destination= 1, Crew = 2, Policy = 3, Calendar = 4, Price = 5};
    class TrackRunner
    {

        FireSharp.FirebaseClient _client;

        TrackConfig _fast_questions =  null;
        TrackConfig _normal_questions = null;
        int _current_question;
        NavigationService _nav_service;
        private ProgressTopHeader _headerPanel;

        Dictionary<int, QuestionType> questionType;
        static TrackRunner _runner;
        private TrackType _trackType;
        private MainWindow mw;
        private BottomPanel bottomPanel = null;
        private TrackConfig _config;
        private string machineId;
        private bool _fast_loaded;
        private bool _normal_loaded;

        public static TrackRunner GetInstance() {
            return _runner;
        }

        TrackRunner(NavigationService navigationService, ProgressTopHeader header)
        {
            _nav_service = navigationService;
           
            _headerPanel = header;
            _headerPanel.Reset();
            mw = Application.Current.MainWindow as MainWindow;
            _client = mw.FClient;
            LoadMachineId();
            LoadQuestions();

        }

        void LoadMachineId() {

            string text = File.ReadAllText(@"C:\\sasystem.config");
           MachineConfig conf = JsonConvert.DeserializeObject<MachineConfig>(text);
            machineId = conf.machineId;

        }

        public static void Init(NavigationService navigationService, ProgressTopHeader header) {
            _runner = new TrackRunner(navigationService, header);
           
        }

        async void LoadNormalTrackQuestions()
        {
            var url = "machines/" + machineId + "/normal";
            try
            {
                FirebaseResponse response = await _client.GetAsync(url);
                string json = response.Body;

                _normal_questions = JsonConvert.DeserializeObject<TrackConfig>(json);
                _normal_loaded = true;
                FinishLoading();
            }
            catch (FirebaseException e) {
                Console.WriteLine(e.Message);

            }
            //string normalJson = File.ReadAllText(@"C:\normal.json");
            //_normal_questions = JsonConvert.DeserializeObject<TrackConfig>(normalJson);


        }

        async void LoadFastTrackQuestions()
        {


            var url = "machines/" + machineId + "/fast";

            FirebaseResponse response = await _client.GetAsync(url);
            string json = response.Body;


            _fast_questions = JsonConvert.DeserializeObject<TrackConfig>(json);


            _fast_loaded = true;

            //_fast_questions = JsonConvert.DeserializeObject<TrackConfig>(json);
            FinishLoading();

        }
        void StartLoading() {
            mw.ShowLoader();
           

        }

        private void FinishLoading()
        {
            if (_fast_loaded && _normal_loaded) {
                mw.HideLoader();
                // questions are loaded, so move on
                _nav_service.Navigate(new Page01());

                if (bottomPanel != null)
                {
                    var grid = bottomPanel?.Parent as Grid;
                    if (grid != null && grid.Children.Contains(bottomPanel) ){
                        grid.Children.Remove(bottomPanel);
                    }
                }

                
            }
        }

        private Page GetQuestionForType(int type)
        {
            var qtype = (QuestionType)type;
            switch (qtype) {
                case QuestionType.CalendarQuestion:
                    {
                        return new CalendarPage();
                        
                    }
                case QuestionType.DestinationQuestion:
                    {
                        return new DestinationSelectionPage();
                    }

                case QuestionType.MedicalConditionsQuestion:{

                        return new Page11();
                    }
                case QuestionType.DetailsQuestion: {
                        return new Page10();
                    }
                case QuestionType.PaymentQuestion: {
                        return new Page09();
                    }
                case QuestionType.ContactQuestion: {
                        return new Page08();
                    }
                case QuestionType.CrewQuestion: {
                        return new Page04();
                  }

                case QuestionType.PolicyQuestion:
                    {
                        return new Page06();
                    }
                case QuestionType.ExtraCoverQuestion:
                    {
                        return new Page07();
                    }
                case QuestionType.ThanksQuestion: {
                        return new ThanksPage();
                    }
                default: {
                        return null;
                    }
            }
        }

       

        public void LoadQuestions() {

            StartLoading();
            _fast_loaded = false;
            _normal_loaded = false;

            LoadNormalTrackQuestions();
            LoadFastTrackQuestions();
        }

        public void StartTrack (TrackType trackType){
            _current_question = 0;
            _trackType = trackType;

            _config = (_trackType == TrackType.NormalTrack) ? _normal_questions : _fast_questions;

            InitBottomPanel();
            _headerPanel.Reset();

            _nav_service.Navigate(GetQuestionForType(_config.questions.ElementAt(_current_question).Value.QuestionType));
            //_nav_service.Navigate(GetQuestionForType(_config.questions[_current_question].Type));


        }

        private void InitBottomPanel()
        {
            
            bottomPanel = new BottomPanel();
            bottomPanel.SetValue(Grid.RowProperty, 1);
            mw.grid.Children.Add(bottomPanel);

        }

        public void NextQuestion() {




            AnimateHeaderPanel(_config.questions.ElementAt(_current_question).Value);

            //AnimateHeaderPanel(_config.questions[_current_question]);
            _current_question++;

            if (_current_question == _config.questions.Count)
            {
                EndTrack();
                return;
            }


            var info = (_config.questions.ElementAt(_current_question).Value);

            //var info = (_config.questions[_current_question]);
            var question = GetQuestionForType(info.QuestionType);
            if (question != null)
            {
                _nav_service.Navigate(question);
                if(info.MovePlane)
                bottomPanel.MovePlane();
            }
            else {
                //BubbleInfoQuestion
                bottomPanel.MovePlaneWithBubble(_config.questions.ElementAt(_current_question).Value.Text);
                //bottomPanel.MovePlaneWithBubble(_config.questions[_current_question].Name);
            }
            
           
        }

        private void EndTrack()
        {
            LoadQuestions();
            _headerPanel.Reset();
            
           
        }

        private void AnimateHeaderPanel(QuestionInfo info)
        {

            _headerPanel.CompletedQuestion(_current_question,info.TabType);  
        }

        private QuestionType GetQuestionType(int question)
        {

            return questionType[question];
        }

        internal bool IsFastTrackRunning()
        {
            return _trackType == TrackType.FastTrack;
        }

        public void CancelTrack() {

            //ask user for cancel
            AskForCancel();
           
        }

        private void AskForCancel()
        {

            var dlg = new CancelConfirmationDialog();
            double width = 1797.5, height = 624.458;
            dlg.SetValue(Canvas.LeftProperty, (1920 - width) / 2);
            dlg.SetValue(Canvas.TopProperty, (1080 - height) / 2);
            var mw = Application.Current.MainWindow as MainWindow;
            dlg.CancelRequested += (e, a) =>
            {

                _current_question = 0;
                _headerPanel.Reset();
                _nav_service.Navigate(new Page01());
                LoadQuestions();
                (bottomPanel?.Parent as Grid).Children.Remove(bottomPanel);

            };

            mw.MainWindowCanvas.Children.Add(dlg);
           
        }
    }
   
}
