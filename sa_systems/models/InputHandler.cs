﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace sa_systems.models
{
    class InputHandler
    {

        static InputHandler _handler = new InputHandler();
        TouchKeyboard _keyboard;
        NumericKeypad _numeric_keypad;
        string _word =String.Empty;
        private string _number = String.Empty;
        bool _isControlPresented = false;
        public InputHandler() {
            MainWindow mw = Application.Current.MainWindow as MainWindow;
            _keyboard = mw.GetTouchKeyboard();
            _numeric_keypad = mw.GetNumericKeypad();
        }

       public  static InputHandler GetInstance() {
            return _handler;
        }
         public void RequestAlphanumericKeyboard(EventHandler doneHandler, string previousText) {

            if (!_isControlPresented)
            {
                _keyboard.Present();
                _keyboard.TypedWord.Content = previousText;
                _keyboard.Done += KeyboardInputDone;
                _keyboard.Done += doneHandler;
                _keyboard.Canceled += (EventArgs, arg) =>
                {
                    _isControlPresented = false;
                };
                _isControlPresented = true;
            }
            
        }

        public String GetLastTypedWord() {
            return _word;
        }
        public String GetLastTypedNumber() {
            return _number;
        }
        private void KeyboardInputDone(object sender, EventArgs e)
        {
            _isControlPresented = false;
          _word =  _keyboard.TypedWord.Content as String;

        }

        public void RequestKeypad(EventHandler doneHandler, string previousText)
        {
            if (!_isControlPresented)
            {
                _numeric_keypad.Present();
                _numeric_keypad.Done += KeypadInputDone;
                _numeric_keypad.Done += doneHandler;
                _numeric_keypad.TypedNumber.Content = previousText;
                _isControlPresented = true;
            }

        }

        private void KeypadInputDone(object sender, EventArgs e)
        {
            _isControlPresented = false;
            _number = _numeric_keypad.TypedNumber.Content as String;
        }

        internal bool IsUIPresented()
        {
            return _isControlPresented;
        }
    }
}
