﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sa_systems.models
{
    class QuestionInfo
    {
        public string Name { get; set;}
        public TabType TabType { get;  set; }
        public int QuestionType { get; set;}
        public bool MovePlane { get; set; }
        public string Text { get; internal set; }
    }
}
