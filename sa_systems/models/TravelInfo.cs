﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sa_systems.models
{

    enum CrewType { Single, Couple, Family, Group}
    enum PolicyType { Silver,Gold};
    class TravelInfo
    {
        private static TravelInfo _currentTravel = new TravelInfo();
        string _destination;
        CrewType _crewType;
        DateTime _startDate, _endDate;
        PolicyType _policy;

        int _adultsCount = 0;
        int _childrenCount = 0;
        TravelInfo() {

        }
        public static TravelInfo GetCurrentTravel() {
            return _currentTravel;

        }
        public  void SetDestination(String destination) {
            _destination = destination;
        }
        public String GetDestination() {
            return _destination;
        }

        public void SetStartDate(DateTime startDate) {
            _startDate = startDate;
        }
        public void SetEndDate(DateTime endDate) {
            _endDate = endDate;
        }

        public string GetCrewTypeString()
        {
            switch (_crewType){
                case CrewType.Single: return "Single";
                case CrewType.Couple: return "Couple";
                case CrewType.Group: return "Group";
                case CrewType.Family: return "Family";
            }
            return "";
            
        }
        public DateTime GetStartDate() {
            return _startDate;
        }
        public DateTime GetEndDate() {
            return _endDate;
        }
        internal int GetAdultsCount()
        {
            return _adultsCount;
           
        }

        internal int GetChildrenCount()
        {
            return _childrenCount;
        }

        public void Reset() {
            //TODO 
        }

        internal string GetPolicyString()
        {
            switch (_policy) {
                case PolicyType.Gold: return "Gold";
                case PolicyType.Silver: return "Silver";
            }
            return "";
        }

        internal void SetCrewType(CrewType crewType)
        {
            _crewType = crewType;
            switch (_crewType) {
                case CrewType.Single:
                    {
                        _adultsCount = 1;
                        _childrenCount = 0;
                    }break;
                case CrewType.Couple: {
                        _adultsCount = 2;

                    }break;
            }
        }

        internal void SetPolicyType(PolicyType policy)
        {
            _policy = policy;
        }
    }
}
