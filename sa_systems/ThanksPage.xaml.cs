﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for ThanksPage.xaml
    /// </summary>
    public partial class ThanksPage : Page
    {
        public ThanksPage()
        {
            InitializeComponent();

            this.Loaded += ThanksPage_Loaded;
        }

        private void ThanksPage_Loaded(object sender, RoutedEventArgs e)
        {
            RunAfterDelay();
        }

        void RunAfterDelay() {

            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(3) };
            timer.Start();
            timer.Tick += (sender, args) =>
            {
                timer.Stop();
                EndQuestion();
            };
        }

        private void EndQuestion()
        {
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (e, a) =>
            {
                TrackRunner.GetInstance().NextQuestion();
            };
            sb.Begin();
        }
    }
}
