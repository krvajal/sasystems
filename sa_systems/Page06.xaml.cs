﻿using sa_systems.models;
using sa_systems.resources.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page06.xaml
    /// </summary>
    public partial class Page06 : Page
    {

        bool showingInfoSheet = false;
        InfoSheet infoSheet;
        public Page06()
        {
            InitializeComponent();
            infoSheet = new InfoSheet();
            infoSheet.Closed += ClosedInfoSheet;
          
            MainSectionContainer.Children.Add(infoSheet);
            HeaderLabel.Content = "Please select a policy for your trip to " + TravelInfo.GetCurrentTravel().GetDestination();
        }

   
        private void ClosedInfoSheet(object sender, EventArgs e) {

            showingInfoSheet = false;

        }

        private void ShowInfoSheet(object sender, MouseButtonEventArgs e)
        {

            if (showingInfoSheet) return;
            infoSheet.SlideDown();
            showingInfoSheet = true;

        }

        private void ShowMore(object sender, RoutedEventArgs e)
        {
            var timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(900) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                timer.Stop();
                var mw = Application.Current.MainWindow as MainWindow;

                var dialog = new ComparisonChartDialog();
                dialog.Width = 1797; //hardcoded from design
                dialog.Height = 968;

                dialog.SetValue(Canvas.LeftProperty, (1920 - dialog.Width) / 2);
                dialog.SetValue(Canvas.TopProperty, (1080 - dialog.Height) / 2);
                mw.MainWindowCanvas.Children.Add(dialog);
                dialog.PerformEnterAnimation();
            };
           


        }

        private void SilverPolicySelected(object sender, RoutedEventArgs e)
        {
            TravelInfo.GetCurrentTravel().SetPolicyType(PolicyType.Silver);
            QuestionDone();
        }

        void QuestionDone() {
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (e, a) => { TrackRunner.GetInstance().NextQuestion(); };
            sb.Begin();

        }

        private void GoldPolicySelected(object sender, RoutedEventArgs e)
        {
            TravelInfo.GetCurrentTravel().SetPolicyType(PolicyType.Gold);
            QuestionDone();
        }
    }
}
