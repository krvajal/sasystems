﻿using sa_systems.resources.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for BottomPanel.xaml
    /// </summary>
    public partial class BottomPanel : UserControl
    {

        int ANIMATIONS_COUNT = 8;
        int count = 0;
        public BottomPanel()
        {
            InitializeComponent();
        }

        private void BackClicked(object sender, RoutedEventArgs e)
        {
       

        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {

            TrackRunner.GetInstance().CancelTrack();


        }

        internal void MovePlane()
        {
            count++;
            if (count <= 0) return;
            if (count > ANIMATIONS_COUNT) return;
            var storyboardName = "AirplaneAnimation" + count;
            var sb = FindResource(storyboardName) as Storyboard;
            sb.Begin();
        }
        public void MovePlaneWithBubble(string bubbleText) {

            count++;
            var bubble = new ProgressBubble();
            bubble.SetMessage(bubbleText);

            var mw = (Application.Current.MainWindow as MainWindow);

            //get position of plane
            Point absolutePoint = new Point(250, 970);
            double aspectRatio = 1.0;
            double bubbleHeight = 414;
            double bubbleWidth = 600;
            double left = absolutePoint.X - bubbleWidth + airplane.Width - 90;
            double top = (absolutePoint.Y - bubbleHeight) * aspectRatio + airplane.Height / 2 + 10;
            bubble.SetValue(Canvas.LeftProperty, left);
            bubble.SetValue(Canvas.TopProperty, top);
            mw.MainWindowCanvas.Children.Add(bubble);
            if (count > ANIMATIONS_COUNT) return;
            var storyboardName = "AirplaneAnimation" + count;
            var sb = FindResource(storyboardName) as Storyboard;

            //create storyboard for bubble translation
            
            DoubleAnimationUsingKeyFrames translateXAnimation = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(translateXAnimation, new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[3].(TranslateTransform.X)"));
            Storyboard.SetTarget(translateXAnimation, bubble);
            //copy keyframes from one to another
            var tmp = (sb.Children[0] as DoubleAnimationUsingKeyFrames);
            foreach (var keyframe in tmp.KeyFrames) {
                var easigKeyFrame = keyframe as EasingDoubleKeyFrame;
                translateXAnimation.KeyFrames.Add( new EasingDoubleKeyFrame( easigKeyFrame.Value, easigKeyFrame.KeyTime));

            }

            DoubleAnimationUsingKeyFrames translateYAnimation = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(translateYAnimation, new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[3].(TranslateTransform.Y)"));
            Storyboard.SetTarget(translateYAnimation, bubble);
            //copy keyframes from one to another
            tmp = (sb.Children[1] as DoubleAnimationUsingKeyFrames);
            foreach (var keyframe in tmp.KeyFrames)
            {
                var easigKeyFrame = keyframe as EasingDoubleKeyFrame;
                translateYAnimation.KeyFrames.Add(new EasingDoubleKeyFrame(easigKeyFrame.Value, easigKeyFrame.KeyTime));

            }


            sb.Children.Add(translateXAnimation);
            sb.Children.Add(translateYAnimation);
            sb.Completed += (ev, arg) =>
            {
                bubble.Close();
                
            };
            sb.Begin();

        }
    }
}
