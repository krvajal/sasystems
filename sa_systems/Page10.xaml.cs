﻿using sa_systems.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page10.xaml
    /// </summary>
    public partial class Page10 : Page
    {

        bool firstNameHasPlaceholder = true;
        bool lastNameHasPlaceholder = true;
        public Page10()
        {
            InitializeComponent();
        }

        private void FirstNameSelected(object sender, RoutedEventArgs e)
        {
            InputHandler.GetInstance().RequestAlphanumericKeyboard(FirstNameTyped,(firstNameHasPlaceholder)?"":FirstName.Text as string );

        }
        private void LastNameSelected(object sender, RoutedEventArgs e)
        {
            InputHandler.GetInstance().RequestAlphanumericKeyboard(LastNameTyped, (lastNameHasPlaceholder) ? "" : LastName.Text as string);

        }

        private void FirstNameTyped(object sender, EventArgs e)
        {
            if (InputHandler.GetInstance().GetLastTypedWord().Length > 0)
            {
                firstNameHasPlaceholder = false;
                FirstName.Text = InputHandler.GetInstance().GetLastTypedWord();
            }
            else {
                firstNameHasPlaceholder = true;
                FirstName.Text = "First name";
            }
        }
        private void LastNameTyped(object sender, EventArgs e)
        {
            if (InputHandler.GetInstance().GetLastTypedWord().Length > 0)
            {
                lastNameHasPlaceholder = false;
                LastName.Text = InputHandler.GetInstance().GetLastTypedWord();
            }
            else
            {
                lastNameHasPlaceholder = true;
                LastName.Text = "Last name";
            }
        }
        private void DaySelected(object sender, EventArgs e) {
            InputHandler.GetInstance().RequestKeypad(DayTyped,"");
        }
        private void MonthSelected(object sender, EventArgs e)
        {
            InputHandler.GetInstance().RequestKeypad(MonthTyped, "");
        }
        private void YearSelected(object sender, EventArgs e)
        {
            InputHandler.GetInstance().RequestKeypad(YearTyped, "");
        }


        private void DayTyped(object sender, EventArgs e)
        {
            string number = InputHandler.GetInstance().GetLastTypedNumber();
            if (number.Length > 0) {
                DayBox.Text = number;
            }
        }
        private void MonthTyped(object sender, EventArgs e)
        {
            string number = InputHandler.GetInstance().GetLastTypedNumber();
            if (number.Length > 0)
            {
                MonthBox.Text = number;
            }
        }
        private void YearTyped(object sender, EventArgs e)
        {
            string number = InputHandler.GetInstance().GetLastTypedNumber();
            if (number.Length > 0)
            {
                YearBox.Text = number;
            }
        }

        private void ConfirmButtonClicked(object sender, RoutedEventArgs e)
        {
            TrackRunner.GetInstance().NextQuestion();

        }

        private void FemaleClicked(object sender, MouseButtonEventArgs e)
        {
            femaleRadioButton.IsChecked = true;
        }


        private void MaleClicked(object sender, MouseButtonEventArgs e)
        {
            maleRadioButton.IsChecked = true;
        }
    }
}
