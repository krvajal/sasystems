﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for OutlineRadioButton.xaml
    /// </summary>
    public partial class OutlineRadioButton : UserControl
    {
        public OutlineRadioButton()
        {
            InitializeComponent();
            this.Loaded += (e, a) =>
            {
                MakeCircleAnimation();
            };
            this.MouseDown += OutlineRadioButton_MouseDown;
        }

        private void OutlineRadioButton_MouseDown(object sender, MouseButtonEventArgs e)
        {


            ellipse.Fill = FindResource("MainBlueSolidColorBrush") as SolidColorBrush;
            text.Foreground = Brushes.White; 
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }


        public static readonly DependencyProperty TextProperty = TextBlock.TextProperty.AddOwner(typeof(OutlineRadioButton));

        void MakeCircleAnimation() {

            var sb = new Storyboard();
            double width = this.Width;
            double perimeter = 3.1415 * width;
            double factor = ellipse.StrokeThickness;
            var collection = new DoubleCollection();
            collection.Add(perimeter/factor);
            ellipse.StrokeDashArray = collection;
            var animation = new DoubleAnimation();
            animation.From =(perimeter / factor);
            animation.To = (0.0);
            animation.Duration = (TimeSpan.FromMilliseconds(1000));
            Storyboard.SetTarget(animation, ellipse);
            Storyboard.SetTargetProperty(animation, new PropertyPath(Ellipse.StrokeDashOffsetProperty));

            sb.Children.Add(animation);
            sb.Begin();

        }
    }
}
