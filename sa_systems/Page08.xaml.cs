﻿using sa_systems.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page08.xaml
    /// </summary>
    public partial class Page08 : Page
    {

        bool emailHasPlaceholder = true;
        bool addressLineHasPlaceholder = true;
        bool zipCodeHasPlacehodler = true;
        public Page08()
        {
            InitializeComponent();
        }

        private void EmailFieldSelected(object sender, RoutedEventArgs e)
        {
            InputHandler.GetInstance().RequestAlphanumericKeyboard(EmailFieldTyped, (emailHasPlaceholder)?"":EmailAddress.Text as String);
        }

        private void EmailFieldTyped(object sender, EventArgs e)
        {
            if (InputHandler.GetInstance().GetLastTypedWord().Length > 0)
            {
                emailHasPlaceholder = false;
                EmailAddress.Text = InputHandler.GetInstance().GetLastTypedWord();
            }
            else
            {
                emailHasPlaceholder = true;
                EmailAddress.Text = "Email address";
            }
        }
        private void AddressFieldSelected(object sender, RoutedEventArgs e)
        {
            InputHandler.GetInstance().RequestAlphanumericKeyboard(AddressFieldTyped, (addressLineHasPlaceholder)?"": AddressLine.Text as String);
        }

        private void AddressFieldTyped(object sender, EventArgs e)
        {
            if (InputHandler.GetInstance().GetLastTypedWord().Length > 0)
            {
                addressLineHasPlaceholder = false;
                AddressLine.Text = InputHandler.GetInstance().GetLastTypedWord();
            }
            else
            {
                addressLineHasPlaceholder = true;
                AddressLine.Text = "First line of address";
            }
        }
      
        private void ZipFieldSelected(object sender, RoutedEventArgs e)
        {
            InputHandler.GetInstance().RequestAlphanumericKeyboard(ZipFieldTyped,(zipCodeHasPlacehodler)?"":ZipCode.Text as String);
        }

        private void ZipFieldTyped(object sender, EventArgs e)
        {
            if (InputHandler.GetInstance().GetLastTypedWord().Length > 0)
            {
                zipCodeHasPlacehodler = false;
                ZipCode.Text = InputHandler.GetInstance().GetLastTypedWord();
            }
            else {
                ZipCode.Text = "Postcode";
            }

        }

        private void ConfirmClicked(object sender, RoutedEventArgs e)
        {
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (ev, a) =>
            {
                TrackRunner.GetInstance().NextQuestion();
            };
            sb.Begin();
        }
    }
}
