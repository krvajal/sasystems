﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for StartTrackButton.xaml
    /// </summary>
    public partial class StartTrackButton : UserControl
    {
        Brush blueBrush;
        public StartTrackButton()
        {
            InitializeComponent();
            SetupAnimations();
        }

        private DoubleAnimation slowRotationAnimation = new DoubleAnimation
        {
            By = -30,
            Duration = TimeSpan.FromMilliseconds(5000),
            RepeatBehavior = RepeatBehavior.Forever,
            IsCumulative = true
        };
        private DoubleAnimation fastRotationAnimation = new DoubleAnimation
        {
            By = -(180 + 360),
            Duration = TimeSpan.FromMilliseconds(300),
            IsCumulative = true,
            EasingFunction = new CircleEase()
        };

        private void SetupAnimations()
        {
            var t1 = TrackText.RenderTransform as RotateTransform;
 
            if (t1 == null)
            {
                double centerX = 420.0 / 2;
                double centerY = 420.0 / 2;
                TrackText.RenderTransform = t1 = new RotateTransform(0, centerX, centerY);
            }
         
            t1.BeginAnimation(RotateTransform.AngleProperty, slowRotationAnimation);
          
        }

        private void GotFocusEvent(object sender, RoutedEventArgs e)
        {
            blueBrush = TrackText.Foreground;
            TrackText.Foreground = Brushes.White;
        }

        private void LostFocusEvent(object sender, RoutedEventArgs e)
        {
            TrackText.Foreground = blueBrush;
        }
        public void SetTrackText(string text) {
            this.TrackText.Text = text;
        }
       
    }
}
