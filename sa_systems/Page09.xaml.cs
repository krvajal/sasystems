﻿using sa_systems.resources.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page09.xaml
    /// </summary>
    public partial class Page09 : Page
    {

        PaymentProcessingErrorDialog dlg;
        public Page09()
        {
            InitializeComponent();
            this.Loaded += Page09_Loaded;
        }

        private void Page09_Loaded(object sender, RoutedEventArgs e)
        {
            CallAffterDelay();
        }

        void CallAffterDelay() {

            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(3) };
            timer.Start();
            timer.Tick += (sender, args) =>
            {
                timer.Stop();
                ShowErrorDialog();
            };
        }
        void ShowErrorDialog() {

            dlg = new PaymentProcessingErrorDialog();
            double width = 1789.1;
            double height = 624.5;
            var mw = Application.Current.MainWindow as MainWindow;
            dlg.DismissedWithOk += TryAgain;
            dlg.DismissedWithCancel += CancelTrack; 
            dlg.SetValue(Canvas.LeftProperty, (1920 - width) / 2);
            dlg.SetValue(Canvas.TopProperty, (1080 - height) / 2);
            mw.MainWindowCanvas.Children.Add(dlg);
        }

        private void CancelTrack(object sender, EventArgs e)
        {
            TrackRunner.GetInstance().CancelTrack();
        }

        void EndQuestion() {
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (ee, ar) =>
            {
                TrackRunner.GetInstance().NextQuestion();
            };
            sb.Begin();
        }
        private void TryAgain(object sender, EventArgs e)
        {
            //when try again just play next question, this is for demo purposes

            EndQuestion();
     
        }
    }
}
