﻿using sa_systems.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for AgeBox.xaml
    /// </summary>
    public partial class AgeBox : UserControl
    {
        public EventHandler AgeEntered;
        public AgeBox()
        {
            InitializeComponent();
        }

        public void BoxSelected(object sender, RoutedEventArgs e)
        {
            InputHandler.GetInstance().RequestKeypad(AgeTyped,NumberBox.Text as String);

        }

        private void AgeTyped(object sender, EventArgs e)
        {
            string number = InputHandler.GetInstance().GetLastTypedNumber();
            NumberBox.Text = number;
            AgeEntered?.Invoke(this, e);
        }
        public void SetTitle(string title) {
             BoxLabel.Text = title;
        }
    }
}
