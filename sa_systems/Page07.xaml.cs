﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page07.xaml
    /// </summary>
    public partial class Page07 : Page
    {
        public Page07()
        {
            InitializeComponent();
        }

        private void ConfirmSelected(object sender, RoutedEventArgs e)
        {

            //run exit animation
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (ev, arg) => { TrackRunner.GetInstance().NextQuestion(); };
            sb.Begin();
          
        }

        private void ClickedCover(object sender, MouseButtonEventArgs e)
        {
            if (Cover1.IsAdded() || Cover2.IsAdded() || Cover3.IsAdded())
            {
                ContinueButtonTextBlock.Text = "Next Step";
            }
            else {
                ContinueButtonTextBlock.Text = "No Thanks!"; 
            }
        }
    }
}
