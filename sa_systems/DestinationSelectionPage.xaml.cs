﻿using sa_systems.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for DestinationSelectionPage.xaml
    /// </summary>
    public partial class DestinationSelectionPage : Page
    {

        bool isCountrySelectorShown = false;
        bool isCountryMainPanelShown = false;
        public DestinationSelectionPage()
        {
            InitializeComponent();
            LoadCountryNames();
        }


        Dictionary<String, List<Tuple<String, String>>> countries;


        private void LoadCountryNames() {
            //for now load here 

            countries = new Dictionary<string, List<Tuple<string, string>>>();

            //A
            List<Tuple<String, String>> A = new List<Tuple<string, string>>();
            A.Add(new Tuple<String, String>("Afghanistan	", "AF"));
            A.Add(new Tuple<String, String>("Åland Islands	", "AX"));
            A.Add(new Tuple<String, String>("Albania	", "AL"));
            A.Add(new Tuple<String, String>("Algeria	", "DZ"));
            A.Add(new Tuple<String, String>("American Samoa	", "AS"));
            A.Add(new Tuple<String, String>("Andorra	", "AD"));
            A.Add(new Tuple<String, String>("Angola	", "AO"));
            A.Add(new Tuple<String, String>("Anguilla	", "AI"));
            A.Add(new Tuple<String, String>("Antarctica	", "AQ"));
            A.Add(new Tuple<String, String>("Antigua and Barbuda	", "AG"));
            A.Add(new Tuple<String, String>("Argentina	", "AR"));
            A.Add(new Tuple<String, String>("Armenia	", "AM"));
            A.Add(new Tuple<String, String>("Aruba	", "AW"));
            A.Add(new Tuple<String, String>("Australia	", "AU"));
            A.Add(new Tuple<String, String>("Austria	", "AT"));
            A.Add(new Tuple<String, String>("Azerbaijan	", "AZ"));


            // B
            List<Tuple<String, String>> B = new List<Tuple<string, string>>();
            B.Add(new Tuple<String, String>("Bahamas	", "BS"));
            B.Add(new Tuple<String, String>("Bahrain	", "BH"));
            B.Add(new Tuple<String, String>("Bangladesh	", "BD"));
            B.Add(new Tuple<String, String>("Barbados	", "BB"));
            B.Add(new Tuple<String, String>("Belarus	", "BY"));
            B.Add(new Tuple<String, String>("Belgium	", "BE"));
            B.Add(new Tuple<String, String>("Belize	", "BZ"));
            B.Add(new Tuple<String, String>("Benin	", "BJ"));
            B.Add(new Tuple<String, String>("Bermuda	", "BM"));
            B.Add(new Tuple<String, String>("Bhutan	", "BT"));
            B.Add(new Tuple<String, String>("Bolivia, Plurinational State of", "BO"));
            B.Add(new Tuple<String, String>("Bonaire, Sint Eustatius and Saba", "BQ"));
            B.Add(new Tuple<String, String>("Bosnia and Herzegovina	", "BA"));
            B.Add(new Tuple<String, String>("Botswana	", "BW"));
            B.Add(new Tuple<String, String>("Bouvet Island	", "BV"));
            B.Add(new Tuple<String, String>("Brazil	", "BR"));
            B.Add(new Tuple<String, String>("British Indian Ocean Territory	", "IO"));
            B.Add(new Tuple<String, String>("Brunei Darussalam	", "BN"));
            B.Add(new Tuple<String, String>("Bulgaria	", "BG"));
            B.Add(new Tuple<String, String>("Burkina Faso	", "BF"));
            B.Add(new Tuple<String, String>("Burundi	", "BI"));

            //C
            List<Tuple<String, String>> C = new List<Tuple<string, string>>();
            C.Add(new Tuple<String, String>("Cambodia	", "KH"));
            C.Add(new Tuple<String, String>("Cameroon	", "CM"));
            C.Add(new Tuple<String, String>("Canada	", "CA"));
            C.Add(new Tuple<String, String>("Cape Verde	", "CV"));
            C.Add(new Tuple<String, String>("Cayman Islands	", "KY"));
            C.Add(new Tuple<String, String>("Central African Republic	", "CF"));
            C.Add(new Tuple<String, String>("Chad	", "TD"));
            C.Add(new Tuple<String, String>("Chile	", "CL"));
            C.Add(new Tuple<String, String>("China	", "CN"));
            C.Add(new Tuple<String, String>("Christmas Island	", "CX"));
            C.Add(new Tuple<String, String>("Cocos (Keeling) Islands	", "CC"));
            C.Add(new Tuple<String, String>("Colombia	", "CO"));
            C.Add(new Tuple<String, String>("Comoros	", "KM"));
            C.Add(new Tuple<String, String>("Congo	", "CG"));
            C.Add(new Tuple<String, String>("Congo, the Democratic Republic of the", "CD"));
            C.Add(new Tuple<String, String>("Cook Islands	", "CK"));
            C.Add(new Tuple<String, String>("Costa Rica	", "CR"));
            C.Add(new Tuple<String, String>("Côte d'Ivoire	", "CI"));
            C.Add(new Tuple<String, String>("Croatia	", "HR"));
            C.Add(new Tuple<String, String>("Cuba	", "CU"));
            C.Add(new Tuple<String, String>("Curaçao	", "CW"));
            C.Add(new Tuple<String, String>("Cyprus	", "CY"));
            C.Add(new Tuple<String, String>("Czech Republic	", "CZ"));

            //D
            List<Tuple<String, String>> D = new List<Tuple<string, string>>();

            D.Add(new Tuple<String, String>("Denmark	", "DK"));
            D.Add(new Tuple<String, String>("Djibouti	", "DJ"));
            D.Add(new Tuple<String, String>("Dominica	", "DM"));
            D.Add(new Tuple<String, String>("Dominican Republic	", "DO"));

            // E
            List<Tuple<String, String>> E = new List<Tuple<string, string>>();

            E.Add(new Tuple<String, String>("Ecuador	", "EC"));
            E.Add(new Tuple<String, String>("Egypt	", "EG"));
            E.Add(new Tuple<String, String>("El Salvador	", "SV"));
            E.Add(new Tuple<String, String>("Equatorial Guinea	", "GQ"));
            E.Add(new Tuple<String, String>("Eritrea	", "ER"));
            E.Add(new Tuple<String, String>("Estonia	", "EE"));
            E.Add(new Tuple<String, String>("Ethiopia	", "ET"));

            // F
            List<Tuple<String, String>> F = new List<Tuple<string, string>>();
            F.Add(new Tuple<String, String>("Falkland Islands (Malvinas)	", "FK"));
            F.Add(new Tuple<String, String>("Faroe Islands	", "FO"));
            F.Add(new Tuple<String, String>("Fiji	", "FJ"));
            F.Add(new Tuple<String, String>("Finland	", "FI"));
            F.Add(new Tuple<String, String>("France	", "FR"));
            F.Add(new Tuple<String, String>("French Guiana	", "GF"));
            F.Add(new Tuple<String, String>("French Polynesia	", "PF"));
            F.Add(new Tuple<String, String>("French Southern Territories	", "TF"));

            //G
            List<Tuple<String, String>> G = new List<Tuple<string, string>>();
            G.Add(new Tuple<String, String>("Gabon	", "GA"));
            G.Add(new Tuple<String, String>("Gambia	", "GM"));
            G.Add(new Tuple<String, String>("Georgia	", "GE"));
            G.Add(new Tuple<String, String>("Germany	", "DE"));
            G.Add(new Tuple<String, String>("Ghana	", "GH"));
            G.Add(new Tuple<String, String>("Gibraltar	", "GI"));
            G.Add(new Tuple<String, String>("Greece	", "GR"));
            G.Add(new Tuple<String, String>("Greenland	", "GL"));
            G.Add(new Tuple<String, String>("Grenada	", "GD"));
            G.Add(new Tuple<String, String>("Guadeloupe	", "GP"));
            G.Add(new Tuple<String, String>("Guam	", "GU"));
            G.Add(new Tuple<String, String>("Guatemala	", "GT"));
            G.Add(new Tuple<String, String>("Guernsey	", "GG"));
            G.Add(new Tuple<String, String>("Guinea	", "GN"));
            G.Add(new Tuple<String, String>("Guinea-Bissau	", "GW"));
            G.Add(new Tuple<String, String>("Guyana	", "GY"));


            //H
            List<Tuple<String, String>> H = new List<Tuple<string, string>>();
            H.Add(new Tuple<String, String>("Haiti	", "HT"));
            H.Add(new Tuple<String, String>("Heard Island and McDonald Islands	", "HM"));
            H.Add(new Tuple<String, String>("Holy See (Vatican City State)	", "VA"));
            H.Add(new Tuple<String, String>("Honduras	", "HN"));
            H.Add(new Tuple<String, String>("Hong Kong	", "HK"));
            H.Add(new Tuple<String, String>("Hungary	", "HU"));


            //I
            List<Tuple<String, String>> I = new List<Tuple<string, string>>();

            I.Add(new Tuple<String, String>("Iceland	", "IS"));
            I.Add(new Tuple<String, String>("India	", "IN"));
            I.Add(new Tuple<String, String>("Indonesia	", "ID"));
            I.Add(new Tuple<String, String>("Iran, Islamic Republic of", "IR"));
            I.Add(new Tuple<String, String>("Iraq	", "IQ"));
            I.Add(new Tuple<String, String>("Ireland	", "IE"));
            I.Add(new Tuple<String, String>("Isle of Man	", "IM"));
            I.Add(new Tuple<String, String>("Israel	", "IL"));
            I.Add(new Tuple<String, String>("Italy	", "IT"));

            //J
            List<Tuple<String, String>> J= new List<Tuple<string, string>>();
            J.Add(new Tuple<String, String>("Jamaica", "JM"));
            J.Add(new Tuple<String, String>("Japan", "JP"));
            J.Add(new Tuple<String, String>("Jersey", "JE"));
            J.Add(new Tuple<String, String>("Jordan", "JO"));



          

            List<Tuple<String, String>> K = new List<Tuple<string, string>>();

            K.Add(new Tuple<String, String>("Kazakhstan	", "KZ"));
            K.Add(new Tuple<String, String>("Kenya	", "KE"));
            K.Add(new Tuple<String, String>("Kiribati	", "KI"));
            K.Add(new Tuple<String, String>("Korea, Democratic People's Republic of", "KP"));
            K.Add(new Tuple<String, String>("Korea, Republic of", "KR"));
            K.Add(new Tuple<String, String>("Kuwait	", "KW"));
            K.Add(new Tuple<String, String>("Kyrgyzstan	", "KG"));

            List<Tuple<String, String>> L = new List<Tuple<string, string>>();

            L.Add(new Tuple<String, String>("Lao People's Democratic Republic	", "LA"));
            L.Add(new Tuple<String, String>("Latvia	", "LV"));
            L.Add(new Tuple<String, String>("Lebanon	", "LB"));
            L.Add(new Tuple<String, String>("Lesotho	", "LS"));
            L.Add(new Tuple<String, String>("Liberia	", "LR"));
            L.Add(new Tuple<String, String>("Libya	", "LY"));
            L.Add(new Tuple<String, String>("Liechtenstein	", "LI"));
            L.Add(new Tuple<String, String>("Lithuania	", "LT"));
            L.Add(new Tuple<String, String>("Luxembourg	", "LU"));

            List<Tuple<String, String>> M = new List<Tuple<string, string>>();
            M.Add(new Tuple<String, String>("Macao	", "MO"));
            M.Add(new Tuple<String, String>("Macedonia, the Former Yugoslav Republic of", "MK"));
            M.Add(new Tuple<String, String>("Madagascar	", "MG"));
            M.Add(new Tuple<String, String>("Malawi	", "MW"));
            M.Add(new Tuple<String, String>("Malaysia	", "MY"));
            M.Add(new Tuple<String, String>("Maldives	", "MV"));
            M.Add(new Tuple<String, String>("Mali	", "ML"));
            M.Add(new Tuple<String, String>("Malta	", "MT"));
            M.Add(new Tuple<String, String>("Marshall Islands	", "MH"));
            M.Add(new Tuple<String, String>("Martinique	", "MQ"));
            M.Add(new Tuple<String, String>("Mauritania	", "MR"));
            M.Add(new Tuple<String, String>("Mauritius	", "MU"));
            M.Add(new Tuple<String, String>("Mayotte	", "YT"));
            M.Add(new Tuple<String, String>("Mexico	", "MX"));
            M.Add(new Tuple<String, String>("Micronesia, Federated States of", "FM"));
            M.Add(new Tuple<String, String>("Moldova, Republic of", "MD"));
            M.Add(new Tuple<String, String>("Monaco	", "MC"));
            M.Add(new Tuple<String, String>("Mongolia	", "MN"));
            M.Add(new Tuple<String, String>("Montenegro	", "ME"));
            M.Add(new Tuple<String, String>("Montserrat	", "MS"));
            M.Add(new Tuple<String, String>("Morocco	", "MA"));
            M.Add(new Tuple<String, String>("Mozambique	", "MZ"));
            M.Add(new Tuple<String, String>("Myanmar	", "MM"));
            M.Add(new Tuple<String, String>("Namibia	", "NA"));


            List<Tuple<String, String>> N = new List<Tuple<string, string>>();


            N.Add(new Tuple<String, String>("Nauru	", "NR"));
            N.Add(new Tuple<String, String>("Nepal	", "NP"));
            N.Add(new Tuple<String, String>("Netherlands	", "NL"));
            N.Add(new Tuple<String, String>("New Caledonia	", "NC"));
            N.Add(new Tuple<String, String>("New Zealand	", "NZ"));
            N.Add(new Tuple<String, String>("Nicaragua	", "NI"));
            N.Add(new Tuple<String, String>("Niger	", "NE"));
            N.Add(new Tuple<String, String>("Nigeria	", "NG"));
            N.Add(new Tuple<String, String>("Niue	", "NU"));
            N.Add(new Tuple<String, String>("Norfolk Island	", "NF"));
            N.Add(new Tuple<String, String>("Northern Mariana Islands	", "MP"));
            N.Add(new Tuple<String, String>("Norway	", "NO"));
            
         

            List<Tuple<String, String>> O = new List<Tuple<string, string>>();
            O.Add(new Tuple<String, String>("Oman	", "OM"));

            List<Tuple<String, String>> P = new List<Tuple<string, string>>();
            P.Add(new Tuple<String, String>("Pakistan	", "PK"));
            P.Add(new Tuple<String, String>("Palau	", "PW"));
            P.Add(new Tuple<String, String>("Palestine, State of", "PS"));
            P.Add(new Tuple<String, String>("Panama	", "PA"));
            P.Add(new Tuple<String, String>("Papua New Guinea	", "PG"));
            P.Add(new Tuple<String, String>("Paraguay	", "PY"));
            P.Add(new Tuple<String, String>("Peru	", "PE"));
            P.Add(new Tuple<String, String>("Philippines	", "PH"));
            P.Add(new Tuple<String, String>("Pitcairn	", "PN"));
            P.Add(new Tuple<String, String>("Poland	", "PL"));
            P.Add(new Tuple<String, String>("Portugal	", "PT"));
            P.Add(new Tuple<String, String>("Puerto Rico	", "PR"));

            List<Tuple<String, String>> Q = new List<Tuple<string, string>>();



            Q.Add(new Tuple<String, String>("Qatar	", "QA"));

            List<Tuple<String, String>> R = new List<Tuple<string, string>>();
            R.Add(new Tuple<String, String>("Réunion	", "RE"));
            R.Add(new Tuple<String, String>("Romania	", "RO"));
            R.Add(new Tuple<String, String>("Russian Federation	", "RU"));
            R.Add(new Tuple<String, String>("Rwanda	", "RW"));

            List<Tuple<String, String>> S = new List<Tuple<string, string>>();

            S.Add(new Tuple<String, String>("Saint Barthélemy	", "BL"));
            S.Add(new Tuple<String, String>("Saint Helena, Ascension and Tristan da Cunha", "SH"));
            S.Add(new Tuple<String, String>("Saint Kitts and Nevis	", "KN"));
            S.Add(new Tuple<String, String>("Saint Lucia	", "LC"));
            S.Add(new Tuple<String, String>("Saint Martin (French part)	", "MF"));
            S.Add(new Tuple<String, String>("Saint Pierre and Miquelon	", "PM"));
            S.Add(new Tuple<String, String>("Saint Vincent and the Grenadines	", "VC"));
            S.Add(new Tuple<String, String>("Samoa	", "WS"));
            S.Add(new Tuple<String, String>("San Marino	", "SM"));
            S.Add(new Tuple<String, String>("Sao Tome and Principe	", "ST"));
            S.Add(new Tuple<String, String>("Saudi Arabia	", "SA"));
            S.Add(new Tuple<String, String>("Senegal	", "SN"));
            S.Add(new Tuple<String, String>("Serbia	", "RS"));
            S.Add(new Tuple<String, String>("Seychelles	", "SC"));
            S.Add(new Tuple<String, String>("Sierra Leone	", "SL"));
            S.Add(new Tuple<String, String>("Singapore	", "SG"));
            S.Add(new Tuple<String, String>("Sint Maarten (Dutch part)	", "SX"));
            S.Add(new Tuple<String, String>("Slovakia	", "SK"));
            S.Add(new Tuple<String, String>("Slovenia	", "SI"));
            S.Add(new Tuple<String, String>("Solomon Islands	", "SB"));
            S.Add(new Tuple<String, String>("Somalia	", "SO"));
            S.Add(new Tuple<String, String>("South Africa	", "ZA"));
            S.Add(new Tuple<String, String>("South Georgia and the South Sandwich Islands	", "GS"));
            S.Add(new Tuple<String, String>("South Sudan	", "SS"));
            S.Add(new Tuple<String, String>("Spain	", "ES"));
            S.Add(new Tuple<String, String>("Sri Lanka	", "LK"));
            S.Add(new Tuple<String, String>("Sudan	", "SD"));
            S.Add(new Tuple<String, String>("Suriname	", "SR"));
            S.Add(new Tuple<String, String>("Svalbard and Jan Mayen	", "SJ"));
            S.Add(new Tuple<String, String>("Swaziland	", "SZ"));
            S.Add(new Tuple<String, String>("Sweden	", "SE"));
            S.Add(new Tuple<String, String>("Switzerland	", "CH"));
            S.Add(new Tuple<String, String>("Syrian Arab Republic	", "SY"));

            List<Tuple<String, String>> T = new List<Tuple<string, string>>();

            T.Add(new Tuple<String, String>("Taiwan, Province of China", "TW"));
            T.Add(new Tuple<String, String>("Tajikistan	", "TJ"));
            T.Add(new Tuple<String, String>("Tanzania, United Republic of", "TZ"));
            T.Add(new Tuple<String, String>("Thailand	", "TH"));
            T.Add(new Tuple<String, String>("Timor-Leste	", "TL"));
            T.Add(new Tuple<String, String>("Togo	", "TG"));
            T.Add(new Tuple<String, String>("Tokelau	", "TK"));
            T.Add(new Tuple<String, String>("Tonga	", "TO"));
            T.Add(new Tuple<String, String>("Trinidad and Tobago	", "TT"));
            T.Add(new Tuple<String, String>("Tunisia	", "TN"));
            T.Add(new Tuple<String, String>("Turkey	", "TR"));
            T.Add(new Tuple<String, String>("Turkmenistan	", "TM"));
            T.Add(new Tuple<String, String>("Turks and Caicos Islands	", "TC"));
            T.Add(new Tuple<String, String>("Tuvalu	", "TV"));

            List<Tuple<String, String>> U = new List<Tuple<string, string>>();
            U.Add(new Tuple<String, String>("Uganda	", "UG"));
            U.Add(new Tuple<String, String>("Ukraine	", "UA"));
            U.Add(new Tuple<String, String>("United Arab Emirates	", "AE"));
            U.Add(new Tuple<String, String>("United Kingdom	", "GB"));
            U.Add(new Tuple<String, String>("United States	", "US"));
            U.Add(new Tuple<String, String>("United States Minor Outlying Islands	", "UM"));
            U.Add(new Tuple<String, String>("Uruguay	", "UY"));
            U.Add(new Tuple<String, String>("Uzbekistan	", "UZ"));

            List<Tuple<String, String>> V = new List<Tuple<string, string>>();

            V.Add(new Tuple<String, String>("Vanuatu	", "VU"));
            V.Add(new Tuple<String, String>("Venezuela, Bolivarian Republic of"	, "VE"));
            V.Add(new Tuple<String, String>("Viet Nam	", "VN"));
            V.Add(new Tuple<String, String>("Virgin Islands, British"	, "VG"));
            V.Add(new Tuple<String, String>("Virgin Islands, U.S.", "VI"));
            List<Tuple<String, String>> W = new List<Tuple<string, string>>();
            W.Add(new Tuple<String, String>("Wallis and Futuna	", "WF"));
            W.Add(new Tuple<String, String>("Western Sahara	", "EH"));
            
            List<Tuple<String, String>> Y = new List<Tuple<string, string>>();


            Y.Add(new Tuple<String, String>("Yemen	", "YE"));
            List<Tuple<String, String>> Z = new List<Tuple<string, string>>();
            Z.Add(new Tuple<String, String>("Zambia	", "ZM"));
            Z.Add(new Tuple<String, String>("Zimbabwe	", "ZW"));

            countries["A"] = A;
            countries["B"] = B;
            countries["C"] = C;
            countries["D"] = D;
            countries["E"] = E;
            countries["F"] = F;
            countries["G"] = G;
            countries["H"] = H;
            countries["I"] = I;
            countries["J"] = J;
            countries["K"] = K;
            countries["L"] = L;
            countries["M"] = M;
            countries["N"] = N;
            countries["O"] = O;
            countries["P"] = P;
            countries["Q"] = Q;
            countries["R"] = R;
            countries["S"] = S;
            countries["T"] = T;
            countries["U"] = U;
            countries["V"] = V;
            countries["W"] = W;
            countries["Y"] = Y;
            countries["Z"] = Z;





        }

        private void ShowCountrySelector(object sender, MouseButtonEventArgs e)
        {
            var animation = new DoubleAnimation {
                From = 0,
                To = 166,
                Duration = TimeSpan.FromMilliseconds(500)
            };

            isCountrySelectorShown = true;
            CountryLettersBottomPanel.BeginAnimation(FrameworkElement.HeightProperty, animation);
        }



        private void HideCountrySelector() {
            var animation = new DoubleAnimation
            {
                From = CountryLettersBottomPanel.ActualHeight,
                To = 0,
                Duration = TimeSpan.FromMilliseconds(500)
            };
            isCountrySelectorShown = false;
            CountryLettersBottomPanel.BeginAnimation(FrameworkElement.HeightProperty, animation);

        }

        private void SelectedEurope(object sender, MouseButtonEventArgs e)
        {
            if (isCountrySelectorShown) {
                HideCountrySelector();

            }
            TravelInfo.GetCurrentTravel().SetDestination("Europe");
            EndQuestion();
        }

        private void SelectedWorldwideNoUsaCan(object sender, MouseButtonEventArgs e)
        {
            if (isCountrySelectorShown)
            {
                HideCountrySelector();

            }
            TravelInfo.GetCurrentTravel().SetDestination("Worlwide (non USA)");
            EndQuestion();
        }

        private void SelectedWorldwide(object sender, MouseButtonEventArgs e)
        {
            if (isCountrySelectorShown)
            {
                HideCountrySelector();

            }
            TravelInfo.GetCurrentTravel().SetDestination("Worldwide");
            EndQuestion();

        }

        private void ShowCountryListForLetter(String letter) {

            double height = 468;
            //load data into fields, SENTIOS STUFF
            CountryList.Children.Clear();
            if (!isCountryMainPanelShown) {
                var animation = new DoubleAnimation
                {
                    From = 0,
                    To = height,
                    Duration = TimeSpan.FromMilliseconds(500)
                };
               

            
                CountryLettersMainPanel.BeginAnimation(FrameworkElement.HeightProperty, animation);
                isCountryMainPanelShown = true;
            }

            foreach (var country in countries[letter])
            {
                Label countryLabel = new Label();
                countryLabel.FontSize = 34;
                countryLabel.Foreground = Brushes.White;
                countryLabel.Content = country.Item1;
                countryLabel.MouseLeftButtonDown += CountryLetterClicked;
                CountryList.Children.Add(countryLabel);
            }
            CountryLetterLabel.Content = letter;



            //TODO: show the country list after the animations ends otherwise
            // the wrap panel tries to rearrange its items and the animation looks bad

        }

        private void CountryLetterClicked(object sender, MouseButtonEventArgs e)
        {
            var label = sender as Label;
            string country = label.Content as String;

            TravelInfo.GetCurrentTravel().SetDestination(country);
            EndQuestion();

        }

        private void LetterSelected(object sender, RoutedEventArgs e)
        {
            var label = sender as RadioButton;
            var countryLetter = label.Content as String;
            ShowCountryListForLetter(countryLetter);
        }

        void EndQuestion() {
            //make slide out animation
            var sb = PagePanel.FindResource("ExitAnimationStoryboard") as Storyboard;
            sb.Completed += ExitAnimationCompleted;
            sb.Begin();
        }

    
       
        private void ExitAnimationCompleted(object sender, EventArgs e) {

            //the page is hidden, go to next one
            TrackRunner.GetInstance().NextQuestion();
        }

       
    }
}
