﻿using sa_systems.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for FamilySetupPanel.xaml
    /// this works for both group and family, but is only for fast track 
    /// so the age is entered using passports
    /// </summary>
    public partial class FamilySetupPanel : UserControl
    {

        List<Image> adultIcons = new List<Image>();
        List<Image> childIcons = new List<Image>();
        List<PassportScanner> adultPassportList = new List<PassportScanner>();
        int scanned_adult_passports = 0;

        //resources
        ImageSource single_scanned_source;
        ImageSource single_outline_source;
        ImageSource single_child_scanned_source;
        ImageSource single_child_outline_source;

        const int MAX_ADULTS = 4; //max number of adutls that can be added, determined by the layout
        const int MAX_CHILDREN = 7;
        private int scanned_child_passports = 0;
        private List<PassportScanner> ChildPassportList = new List<PassportScanner>();

        public FamilySetupPanel()
        {


            InitializeComponent();
            InitImageSources();
            AddAdultIcon();



            SetLabel();

        }
        public void SetLabel()
        {
            TitleLabel.Content = TravelInfo.GetCurrentTravel().GetCrewTypeString();

        }

        private void AddAdultIcon() {
            Image adultImage = new Image();
            adultImage.Width = 90;
            adultImage.Height = 160;
            adultImage.Source = single_scanned_source;
            this.AdultsIconsList.Children.Add(adultImage);
            this.adultIcons.Add(adultImage);
            var passportScanner = new PassportScanner();
            this.adultPassportList.Add(passportScanner);
            this.AdultsPassportList.Children.Add(passportScanner);
            UpdateAdultScanButton();
            UpdateChildScanButton();

        }

        private void AddChildIcon()
        {
            Image childImage = new Image();
            childImage.Width = 50;
            childImage.Height = 61;
            childImage.Source = single_child_scanned_source;
            this.ChildIconsList.Children.Add(childImage);
            this.childIcons.Add(childImage);
            var passportScanner = new PassportScanner();
            this.ChildPassportList.Add(passportScanner);
            this.ChildrenPassportList.Children.Add(passportScanner);
            UpdateChildScanButton();

        }

        private void InitImageSources()
        {
            single_scanned_source = (FindResource("image_single_scanned") as Image).Source;
            single_outline_source = (FindResource("image_single_outline") as Image).Source;
            single_child_outline_source = (FindResource("image_single_child_outline") as Image).Source;
            single_child_scanned_source = (FindResource("image_single_child_scanned") as Image).Source;
        }

        private void ScanPassport(object sender, RoutedEventArgs e)
        {
            if (scanned_adult_passports >= this.AdultsIconsList.Children.Count) return;
            //do sentios stuff here
            // when done
            this.adultPassportList.ElementAt(scanned_adult_passports).PerformScanAnimation();
            scanned_adult_passports++;

            UpdateAdultScanButton();
        }

        void UpdateAdultScanButton()
        {
            if (scanned_adult_passports < this.AdultsPassportList.Children.Count)
            {
                this.ScanAdultPassportButtonLabel.Text = "SCAN ADULT " + (scanned_adult_passports + 1) + " PASSPORT";
                this.ScanAdultPassportButton.Visibility = Visibility.Visible;
            }
            else {
                this.ScanAdultPassportButton.Visibility = Visibility.Hidden;
            }
            this.AdultCountLabel.Text = this.adultIcons.Count + " adults";

        }
        private void AddAdult(object sender, RoutedEventArgs e)
        {
            if (adultIcons.Count == MAX_ADULTS) return;
            AddAdultIcon();
            UpdateAdultScanButton();
          
        }
        private void AddChild(object sender, RoutedEventArgs e)
        {
            if (childIcons.Count == MAX_CHILDREN) return;
            AddChildIcon();
            UpdateChildScanButton();

        }

        private void UpdateChildScanButton()
        {
            if (scanned_child_passports < this.ChildPassportList.Count)
            {
                this.ScanChildPassportButtonLabel.Text = "SCAN CHILD " + (scanned_child_passports + 1) + " PASSPORT";
                this.ScanChildPassportButton.Visibility = Visibility.Visible;
            }
            else
            {
                this.ScanChildPassportButton.Visibility = Visibility.Hidden;
            }
          
            this.ChildCountLabel.Text = this.childIcons.Count + " children";

        }

        private void RemoveAdult(object sender, RoutedEventArgs e)
        {
            if (adultIcons.Count == 0) return;
            this.AdultsIconsList.Children.Clear();
            adultIcons.RemoveAt(adultIcons.Count - 1);
            RemoveAdultPassport();
            
            foreach( var img in adultIcons)
            {
                this.AdultsIconsList.Children.Add(img);
            }
            UpdateAdultScanButton();

        }

        private void RemoveAdultPassport()
        {
            if (AdultsPassportList.Children.Count > 0)
            {
                if (scanned_adult_passports == AdultsPassportList.Children.Count)
                    scanned_adult_passports--;
                this.adultPassportList.RemoveAt(this.AdultsPassportList.Children.Count - 1);
                this.AdultsPassportList.Children.RemoveAt(AdultsPassportList.Children.Count - 1);
                
            }
            
        }

        private void ConfirmClicked(object sender, RoutedEventArgs e)
        {

            //check input here maybe?
            TrackRunner.GetInstance().NextQuestion();
        }

        private void RemoveChild(object sender, RoutedEventArgs e)
        {

            if (childIcons.Count == 0) return;
            this.ChildIconsList.Children.Clear();
            childIcons.RemoveAt(childIcons.Count - 1);
            RemoveChildPassport();

            foreach (var img in childIcons)
            {
                this.ChildIconsList.Children.Add(img);
            }
            UpdateChildScanButton();

        }

        private void RemoveChildPassport()
        {
            if (ChildrenPassportList.Children.Count > 0)
            {
                if (scanned_child_passports == ChildrenPassportList.Children.Count)
                    scanned_child_passports--;
                this.ChildPassportList.RemoveAt(this.AdultsPassportList.Children.Count - 1);
                this.ChildrenPassportList.Children.RemoveAt(ChildrenPassportList.Children.Count - 1);

            }

        }

        private void ScanChildPassport(object sender, RoutedEventArgs e)
        {

            if (scanned_child_passports >= this.ChildIconsList.Children.Count) return;
            //do sentios stuff here
            // when done
            this.ChildPassportList.ElementAt(scanned_child_passports).PerformScanAnimation();
            scanned_child_passports++;

            UpdateChildScanButton();


        }
    }
}
