﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for ProgressBubble.xaml
    /// </summary>
    public partial class ProgressBubble : UserControl
    {

        string text;
        public ProgressBubble()
        {
            InitializeComponent();
        }
        public string CaptionText
        {
            get { return (string)GetValue(CaptionTextProperty); }
            set { SetValue(CaptionTextProperty, value); }
        }
        public static readonly DependencyProperty CaptionTextProperty = TextBlock.TextProperty.AddOwner(typeof(ProgressBubble));

        internal void Close()
        {
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (e, a) =>
            {
                (this.Parent as Canvas).Children.Remove(this);
                TrackRunner.GetInstance().NextQuestion();
            };
            sb.Begin();
        }
       public void SetMessage(string txt) {
            Message.Text = txt;
            this.text = txt;
        }

    }
}
