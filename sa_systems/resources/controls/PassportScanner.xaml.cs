﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for PassportScanner.xaml
    /// </summary>
    public partial class PassportScanner : UserControl
    {

        bool scanned = false;
        public PassportScanner()
        {
            InitializeComponent();
        }
        public void PerformScanAnimation() {
            if (scanned == true) return;
            var sb = FindResource("ScanAnimationStoryboard") as Storyboard;
            sb.Completed += ScanAnimationCompleted;
            sb.Begin();
        }

        private void ScanAnimationCompleted(object sender, EventArgs e)
        {
        

            front_image.Source = (FindResource("passport_checked_image") as Image).Source;
            scanned = true;
            //notify called
        }
    }
   
}
