﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for PersonIconCheckbox.xaml
    /// </summary>
    public partial class PersonIconCheckbox : UserControl
    {

        public EventHandler Checked;
        public PersonIconCheckbox()
        {
            InitializeComponent();
        }


        public  string GroupName
        {
            get { return (string)GetValue(GroupNameProperty); }
            set { SetValue(GroupNameProperty, value); }
        }

        public static readonly DependencyProperty GroupNameProperty = RadioButton.GroupNameProperty.AddOwner(typeof(PersonIconCheckbox));

        public void SetDone() {
            Radio.SetValue(RadioButton.IsCheckedProperty, null);
        }

        internal bool IsSelected()
        {
            return Radio.IsChecked == true;
        }

        private void PersonChecked(object sender, RoutedEventArgs e)
        {
            Checked?.Invoke(this, e);
        }
    }
}
