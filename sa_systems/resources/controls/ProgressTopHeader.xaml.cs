﻿using sa_systems.models;
using sa_systems.resources.controls.ProgressTabs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for ProgressTopHeader.xaml
    /// </summary>
    public partial class ProgressTopHeader : UserControl
    {

        const double HEAD_WIDTH = 64;
        const double PROGRESS_UNIT = 266.8;
        const double OFFSET = 400;
        int zindex = 0;
        public ProgressTopHeader()
        {
            InitializeComponent();
        }

        public void Reset() {
            grid.Children.Clear();
            zindex = 0;
            
        }

        internal void CompletedQuestion(int questionNumber, TabType tabType)
        {
            if (grid.Children.Count > 5) return;

            zindex = zindex - 1;
            if (grid.Children.Count == 0) {
                var logo = new Logo();
                logo.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
                grid.Children.Add(logo);
            }
            

            TravelInfo info = TravelInfo.GetCurrentTravel();

            int current_count = grid.Children.Count - 1;
            double start = OFFSET + current_count * PROGRESS_UNIT;
            start = start - (PROGRESS_UNIT + HEAD_WIDTH);
            
            switch (tabType) {

                case TabType.Destination: {

                        var tab = new ProgressTab();

                        tab.SetDestination(info.GetDestination());
                        SetInitialPosition(start, tab);
                        grid.Children.Add(tab);
                        break;
                    }
                case TabType.Calendar: {

                        var tab = new CalendarProgressTab();
                        tab.SetStartDate(info.GetStartDate());
                        tab.SetEndDate(info.GetEndDate());
                        SetInitialPosition(start, tab);
                        grid.Children.Add(tab);
                        break;
                    }
                case TabType.Policy:                  {
                        var tab = new PolicyCategoryTab();
                        tab.SetPolicyType(info.GetPolicyString());
                        SetInitialPosition(start, tab);
                        grid.Children.Add(tab);
                        break;
                    }
                case TabType.Crew: {

                        var tab = new CrewProgressTab();
                        tab.SetCrewType(info.GetCrewTypeString());
                        tab.SetAdultsCount(info.GetAdultsCount());
                        tab.SetChildrenCount(info.GetChildrenCount());
                        SetInitialPosition(start, tab);
                        grid.Children.Add(tab);
                        break;
                    }

                case TabType.Price:
                    {
                        var tab = new TotalPriceTab();
                        SetInitialPosition(start, tab);
                        grid.Children.Add(tab);
                        break;
                    }
                case TabType.None: {
                        //noop
                        break;
                  }

            }
           


        }

        private void SetInitialPosition(double start, UserControl tab)
        {
            tab.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
            TranslateTransform trans = new TranslateTransform(start, 0);
            tab.RenderTransform = trans;
            DoubleAnimation animation = new DoubleAnimation {
                By = PROGRESS_UNIT + HEAD_WIDTH,
                Duration = TimeSpan.FromMilliseconds(300)
            };
            Grid.SetZIndex(tab, zindex);
            trans.BeginAnimation(TranslateTransform.XProperty, animation);

        }
    }
}
