﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for PaymentProcessingErrorDialog.xaml
    /// </summary>
    /// 
  
    public partial class PaymentProcessingErrorDialog : UserControl
    {
        public EventHandler DismissedWithOk;
        public EventHandler DismissedWithCancel;
        bool _tryAgain;
        public PaymentProcessingErrorDialog()
        {
            InitializeComponent();
        }

        private void NoButtonClicked(object sender, RoutedEventArgs e)
        {
            _tryAgain = false;
            ExitDialog();
          
        }

        private void YesButtonClicked(object sender, RoutedEventArgs e)
        {
            _tryAgain = true;
            ExitDialog();
           
        }
        void ExitDialog() {
             var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += (e, a) =>
            {
                if (_tryAgain)
                    DismissedWithOk?.Invoke(this, a);
                else {
                    DismissedWithCancel?.Invoke(this, a);
                }
                (this.Parent as Canvas).Children.Remove(this);
                
            };
            sb.Begin();
            
        }
    }
}
