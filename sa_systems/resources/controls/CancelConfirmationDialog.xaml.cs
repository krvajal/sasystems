﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for CancelConfirmationDialog.xaml
    /// </summary>
    public partial class CancelConfirmationDialog : UserControl
    {


        public event EventHandler CancelRequested;
        bool cancelTrack = false;
        public CancelConfirmationDialog()
        {
            InitializeComponent();
        }

        private void YesClicked(object sender, RoutedEventArgs e)
        {
            cancelTrack = true;

            RunExitAnimation();
        }

        private void NoClicked(object sender, RoutedEventArgs e)
        {
            //todo: animate exit
            RunExitAnimation();

        }

        void RunExitAnimation() {
            var sb = FindResource("ExitAnimation") as Storyboard;
            sb.Completed += Sb_Completed;
            sb.Begin();

        }

        private void Sb_Completed(object sender, EventArgs e)
        {
            if (cancelTrack) {

                CancelRequested?.Invoke(this, e);
            }
            (this.Parent as Canvas).Children.Remove(this);
        }
    }
}
