﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for TouchResponseControl.xaml
    /// </summary>
    public partial class TouchResponseControl : UserControl
    {
        public TouchResponseControl()
        {
            InitializeComponent();

        }
        public void Blink() {
            var sb = FindResource("AnimationStoryboard") as Storyboard;
            sb.Completed += AnimationCompleted;
            sb.Begin();
            
        }

        private void AnimationCompleted(object sender, EventArgs e)
        {
           var canvas = this.Parent as Canvas;
            canvas.Children.Remove(this);
        }
    }
}
