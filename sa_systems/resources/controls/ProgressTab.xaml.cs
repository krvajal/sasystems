﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for ProgressTab.xaml
    /// </summary>
    public partial class ProgressTab : UserControl
    {
        public ProgressTab()
        {
            InitializeComponent();
        }
       public void SetDestination(string destination) {
            DestinatonLabel.Text = destination;
        }
    }
}
