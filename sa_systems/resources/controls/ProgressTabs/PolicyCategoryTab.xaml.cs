﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls.ProgressTabs
{
    /// <summary>
    /// Interaction logic for PolicyCategoryTab.xaml
    /// </summary>
    public partial class PolicyCategoryTab : UserControl
    {
        public PolicyCategoryTab()
        {
            InitializeComponent();
        }

        internal void SetPolicyType(string p)
        {
            PolicyTypeLabel.Content = p.ToUpper();
        }

        internal void SetPolicyType(object v)
        {
            throw new NotImplementedException();
        }
    }
}
