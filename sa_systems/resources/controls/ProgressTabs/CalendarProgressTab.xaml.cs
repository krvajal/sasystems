﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls.ProgressTabs
{
    /// <summary>
    /// Interaction logic for CalendarProgressTab.xaml
    /// </summary>
    public partial class CalendarProgressTab : UserControl
    {
        int _num_days;
        DateTime _start;
        DateTime _end;
        public CalendarProgressTab()
        {
            InitializeComponent();
        }
       public void SetStartDate(DateTime date)
        {
            _start = date;
          StartDateLabel.Content = date.ToString("MMM dd", CultureInfo.InvariantCulture);
            UpdateNumDays();
        }
        public void SetEndDate(DateTime date) {
            _end = date;
            EndDateLabel.Content = date.ToString("MMM dd", CultureInfo.InvariantCulture);
            UpdateNumDays();
        }

        void UpdateNumDays() {

            TimeSpan span = _end - _start;

            _num_days = (int)span.TotalDays + 2 ;
            Title.Content = _num_days + " DAYS";
        }
    }
}
