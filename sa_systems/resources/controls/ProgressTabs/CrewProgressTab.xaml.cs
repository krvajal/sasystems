﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls.ProgressTabs
{
    /// <summary>
    /// Interaction logic for CrewProgressTab.xaml
    /// </summary>
    public partial class CrewProgressTab : UserControl
    {
        public CrewProgressTab()
        {
            InitializeComponent();
            AdultsLabel.Content = String.Empty;
            ChildrenLabel.Content = String.Empty;

        }
       public void SetCrewType(string type) {

            CrewTypeLabel.Content = type.ToUpper();

        }
       public  void SetAdultsCount(int count) {
            if (count < 0) return;
            AdultsLabel.Content = count + " adult" + ((count>0)?"s":"");
            

        }
        public void SetChildrenCount(int count) {
            if (count < 0) return;
            ChildrenLabel.Content = count + " children";
        }
    }
}
