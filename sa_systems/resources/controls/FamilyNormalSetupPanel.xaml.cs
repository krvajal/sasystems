﻿using sa_systems.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for FamilyNormalSetupPanel.xaml
    /// the age is entered by hand
    /// </summary>
    public partial class FamilyNormalSetupPanel : UserControl
    {
       

            List<Image> adultIcons = new List<Image>();
            List<PassportScanner> adultPassportList = new List<PassportScanner>();
            int scanned_passports = 0;

            //resources
            ImageSource single_scanned_source;
            ImageSource single_outline_source;
            ImageSource single_child_scanned_source;
            ImageSource single_child_outline_source;

            const int MAX_ADULTS = 4; //max number of adutls that can be added, determined by the layout
         const int MAX_CHILDREN = 4;
        public FamilyNormalSetupPanel()
        {


            InitializeComponent();

            UpdateChildrenLabel();
            UpdateAdultsLabel();
            AdultsPlusClicked(this,null); // add one by default

            

        }
       


        private void ConfirmClicked(object sender, RoutedEventArgs e)
        {

            //check input here maybe?
            TrackRunner.GetInstance().NextQuestion();
        }

        private void ChildrenPlusClicked(object sender, RoutedEventArgs e)
        {
            if (ChildrenWrapPanel.Children.Count < MAX_CHILDREN) {
                var box = new PersonIconCheckbox();
                box.Checked += (ev, a) =>
                {
                    int index = ChildrenWrapPanel.Children.IndexOf(box);
                    var ageBox =(ChildrenAgesWrapPanel.Children[index] as AgeBox);
                    ageBox.BoxSelected(this, e);
                };
                box.SetValue(PersonIconCheckbox.GroupNameProperty, "ChildrenGroup");
                ChildrenWrapPanel.Children.Add(box);
                AddChildrenAgeBox();
                 
            }
        }
        void AddChildrenAgeBox() {
            var box = new AgeBox();
            ChildrenAgesWrapPanel.Children.Add(box);
            box.SetTitle("Child " + ChildrenAgesWrapPanel.Children.Count + " age:");
            box.AgeEntered += (e, a) =>
            {
                int index = ChildrenAgesWrapPanel.Children.IndexOf(box);
                var icon = (ChildrenWrapPanel.Children[index] as PersonIconCheckbox);
                icon.SetValue(FrameworkElement.IsEnabledProperty, false);

            };
            UpdateChildrenLabel();
        }
        private void ChildrenMinusClicked(object sender, RoutedEventArgs e)
        {
            if (!InputHandler.GetInstance().IsUIPresented())
                if (ChildrenWrapPanel.Children.Count > 0) {

                ChildrenWrapPanel.Children.RemoveAt(ChildrenWrapPanel.Children.Count - 1);
                RemoveChildrenAgeBox();
            }
        }

        private void RemoveChildrenAgeBox()
        {
            ChildrenAgesWrapPanel.Children.RemoveAt(ChildrenAgesWrapPanel.Children.Count - 1);
            UpdateChildrenLabel();
        }
        private void UpdateChildrenLabel() {
            int count = ChildrenAgesWrapPanel.Children.Count;
            ChildrenCountLabel.Text = count + " children"; 

        }
        // this is bad practice, I know, I know but the time is runnig (I PROMESS WILL CHANGE)
        private void AdultsPlusClicked(object sender, RoutedEventArgs e)
        {
            if (AdultsWrapPanel.Children.Count < MAX_ADULTS)
            {
                var box = new PersonIconCheckbox();
                box.Checked += (ev, a) =>
                {
                    int index = AdultsWrapPanel.Children.IndexOf(box);
                    var ageBox = (AdultsAgesWrapPanel.Children[index] as AgeBox);
                    ageBox.BoxSelected(this, e);
                };
                box.SetValue(PersonIconCheckbox.GroupNameProperty, "AdultsGroup");
                AdultsWrapPanel.Children.Add(box);
                AddAdultsAgeBox();

            }
        }
        void AddAdultsAgeBox()
        {
            var box = new AgeBox();
            AdultsAgesWrapPanel.Children.Add(box);
            box.SetTitle("Adult " + AdultsAgesWrapPanel.Children.Count + " age:");
            box.AgeEntered += (e, a) =>
            {
                int index = AdultsAgesWrapPanel.Children.IndexOf(box);
                var icon = (AdultsWrapPanel.Children[index] as PersonIconCheckbox);
                icon.SetValue(FrameworkElement.IsEnabledProperty, false);

            };
            UpdateAdultsLabel();
        }
        private void AdultsMinusClicked(object sender, RoutedEventArgs e)
        {
            if(!InputHandler.GetInstance().IsUIPresented())

            if (AdultsWrapPanel.Children.Count > 1)
            {

                AdultsWrapPanel.Children.RemoveAt(AdultsWrapPanel.Children.Count - 1);
                RemoveAdultsAgeBox();
            }
        }

        private void RemoveAdultsAgeBox()
        {
            AdultsAgesWrapPanel.Children.RemoveAt(AdultsAgesWrapPanel.Children.Count - 1);
            UpdateAdultsLabel();
        }
        private void UpdateAdultsLabel()
        {
            int count = AdultsAgesWrapPanel.Children.Count;
            AdultsCountLabel.Text = count + " Adults";

        }

    }
}
