﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for MedicalConditionsInfoSheet.xaml
    /// </summary>
    public partial class MedicalConditionsInfoSheet : UserControl
    {
        public event EventHandler Closed;
        public MedicalConditionsInfoSheet()
        {
            InitializeComponent();
            //init with zero height

        }
        public void SlideDown()
        {
            var sb = FindResource("SlideDownAnimationStoryboard") as Storyboard;
            sb.Begin();

        }
        public void SlideUp()
        {
            var sb = FindResource("SlideUpAnimationStoryboard") as Storyboard;
            sb.Completed += SlideUpCompleted;
            sb.Begin();
        }

        private void SlideUpCompleted(object sender, EventArgs e)
        {

            Closed?.Invoke(this, e);
        }

        private void CloseSheet(object sender, RoutedEventArgs e)
        {
            SlideUp();
        }
    }
}
