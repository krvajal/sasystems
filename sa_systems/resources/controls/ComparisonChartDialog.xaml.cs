﻿using sa_systems.AttachedBehaviors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace sa_systems.resources.controls
{
    /// <summary>
    /// Interaction logic for ComparisonChartDialog.xaml
    /// </summary>
    public partial class ComparisonChartDialog : UserControl
    {
        public ComparisonChartDialog()
        {
            InitializeComponent();
        }

        private void ScrollDown(object sender, RoutedEventArgs e)
        {
            double height = this.ContentScroller.Height;
            var scrollViewer = this.ContentScroller;
            DoubleAnimation verticalAnimation = new DoubleAnimation();

            verticalAnimation.From = scrollViewer.VerticalOffset;
            verticalAnimation.To = scrollViewer.VerticalOffset + height;
            verticalAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(300));

            Storyboard storyboard = new Storyboard();

            storyboard.Children.Add(verticalAnimation);
            Storyboard.SetTarget(verticalAnimation, scrollViewer);
            Storyboard.SetTargetProperty(verticalAnimation, new PropertyPath(ScrollAnimationBehavior.VerticalOffsetProperty)); // Attached dependency property
            storyboard.Begin();
        }
        public void PerformEnterAnimation() {
            var sb = grid.FindResource("ScaleInAnimationStoryboard") as Storyboard;
            sb.Begin(); 
        }

        private void Close(object sender, RoutedEventArgs e)
        {

            var timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(300) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                timer.Stop();
                var sb = FindResource("ExitAnimation") as Storyboard;
                sb.Completed += (ee, a) =>
                {
                    var mw = Application.Current.MainWindow as MainWindow;
                    if (mw.MainWindowCanvas.Children.Contains(this))
                    {
                        mw.MainWindowCanvas.Children.Remove(this);
                    }
                };
                sb.Begin();
            };
           
        }
    }
}
