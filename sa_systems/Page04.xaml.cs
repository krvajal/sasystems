﻿using sa_systems.models;
using sa_systems.resources.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sa_systems
{
    /// <summary>
    /// Interaction logic for Page04.xaml
    /// </summary>
    public partial class Page04 : Page
    {
        public Page04()
        {
            InitializeComponent();

            var sb = new Storyboard();
            var animation = new DoubleAnimation
            {
                From = 90,
                To = 0,
                BeginTime = TimeSpan.FromMilliseconds(500),
                Duration = TimeSpan.FromMilliseconds(1000)
            };
            var opacityAnimation = new DoubleAnimation
            {
                From = 0,
                To = 1,
                BeginTime = TimeSpan.FromMilliseconds(500),
                EasingFunction = new CubicEase(),
                Duration = TimeSpan.FromMilliseconds(1000)

            };

            Storyboard.SetTarget(animation, RectangleExpander);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Height"));
            Storyboard.SetTarget(opacityAnimation, RectangleExpander);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath("Opacity"));

            sb.Children.Add(animation);
            sb.Children.Add(opacityAnimation);

            sb.Begin();

        }


        private void SingleChecked(object sender, MouseButtonEventArgs e)
        {
            TravelInfo.GetCurrentTravel().SetCrewType(CrewType.Single);

            TrackRunner.GetInstance().NextQuestion();
        }

        private void CoupleChecked(object sender, MouseButtonEventArgs e)
        {
            TravelInfo.GetCurrentTravel().SetCrewType(CrewType.Couple);
            TrackRunner.GetInstance().NextQuestion();
        }

        private void FamilyChecked(object sender, MouseButtonEventArgs e)
        {
            TravelInfo.GetCurrentTravel().SetCrewType(CrewType.Family);
            CollectMembersInfo();
        }
        void CollectMembersInfo()
        {

            if (TrackRunner.GetInstance().IsFastTrackRunning())
            {
                var familySetupPanel = new FamilySetupPanel();
                familySetupPanel.SetValue(Grid.ColumnSpanProperty, 5);
                familySetupPanel.SetValue(Grid.RowSpanProperty, 3);
                CrewSelectionPanel.Children.Add(familySetupPanel);
            }
            else
            {

                var familySetupPanel = new FamilyNormalSetupPanel();
                familySetupPanel.SetValue(Grid.ColumnSpanProperty, 5);
                familySetupPanel.SetValue(Grid.RowSpanProperty, 3);
                CrewSelectionPanel.Children.Add(familySetupPanel);
            }
        }


        private void GroupChecked(object sender, MouseButtonEventArgs e)
        {
            TravelInfo.GetCurrentTravel().SetCrewType(CrewType.Group);

            CollectMembersInfo();


        }
    }
}
